DELIMITER $$

DROP TRIGGER /*!50032 IF EXISTS */ `tracking_update_bankaccount`$$

CREATE TRIGGER `tracking_update_bankaccount` AFTER UPDATE ON `fundbankaccount` 
    FOR EACH ROW BEGIN
        IF (new.fundid <> old.fundid OR new.BankAccountTypeID <> old.BankAccountTypeID  OR IFNULL(new.FundBankAccountBSB,'') <> IFNULL(old.FundBankAccountBSB,'')
       OR IFNULL(new.FundBankAccountNo,'') <> IFNULL(old.FundBankAccountNo,'')  OR IFNULL(new.IsClientAdded,'') <> IFNULL(old.IsClientAdded,'') OR IFNULL(new.IsDeleted,'') <> IFNULL(old.IsDeleted,'')
       OR IFNULL(new.IsExpired,'') <> IFNULL(old.IsExpired,'') OR IFNULL(new.ExpirationDate,'') <> IFNULL(old.ExpirationDate,'')  OR IFNULL(new.CreateTime,'') <> IFNULL(old.CreateTime,'') OR IFNULL(new.OrderIndex,'') <> IFNULL(old.OrderIndex,'') 
       OR IFNULL(new.AccountRestriction,'') <> IFNULL(old.AccountRestriction,'') OR IFNULL(new.TFNwithhold,'') <> IFNULL(old.TFNwithhold,'') OR IFNULL(new.CurrentBalance,'') <> IFNULL(old.CurrentBalance,'')
       OR IFNULL(new.BalanceDate,'') <> IFNULL(old.BalanceDate,'') OR IFNULL(new.Onhold,'') <> IFNULL(old.Onhold,'') OR IFNULL(new.IntRate,'') <> IFNULL(old.IntRate,'') OR IFNULL(new.Principal,'') <> IFNULL(old.Principal,'')
       OR IFNULL(new.IsAuthorised,'') <> IFNULL(old.IsAuthorised,'')  OR IFNULL(new.AccountName,'') <> IFNULL(old.AccountName,'')
     )
     THEN
        INSERT INTO fund_change_tracking(table_name,action_flag,source_id,action_time)
        VALUES('fundbankaccount','U',new.Id,NOW());
     END IF;
END;
$$

DELIMITER ;