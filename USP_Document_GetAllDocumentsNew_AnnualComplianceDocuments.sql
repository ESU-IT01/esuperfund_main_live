DELIMITER $$

USE `esuperfund_main_live`$$

DROP PROCEDURE IF EXISTS `USP_Document_GetAllDocumentsNew_AnnualComplianceDocuments`$$

CREATE PROCEDURE `USP_Document_GetAllDocumentsNew_AnnualComplianceDocuments`(v_fundid INT,v_ayear INT)
BEGIN
	DECLARE v_fyid INT DEFAULT 0;
	DECLARE v_fy INT DEFAULT 0;
	DECLARE v_checkliststatus VARCHAR(100) DEFAULT '';
	DECLARE v_lodgeid INT DEFAULT 0;
	DECLARE v_isneedac INT DEFAULT 0;
	DECLARE v_compliancereviewcount INT DEFAULT 0;
	DECLARE v_accountantid VARCHAR(10) DEFAULT 0;
	DECLARE v_isshow INT DEFAULT 0;

	DECLARE v_cgtfilecount INT DEFAULT 0;
	SELECT id,`year`,ChecklistStatus,LodgementStatusID,IFNULL(IsPensionNeedAC,0),AccountantID,IFNULL(GSTLodgeRequired,0) INTO v_fyid,v_fy,v_checkliststatus,v_lodgeid,v_isneedac,v_accountantid,v_isshow
	FROM cl_fy_fundfinancialyear WHERE fundid=v_fundid AND `year`= v_ayear;
	IF v_fyid>0 THEN
		/* added by Dwyane, 6/8/2015, adminend-561*/
		SELECT COUNT(*) INTO v_compliancereviewcount FROM wip_fy_reviewsheet WHERE ReviewItemID IN(20,42,58) AND fyid=v_fyid AND YesOrNo=1;
		IF v_compliancereviewcount=1 THEN
			INSERT INTO temp_documents_table(t_date,t_documenttype,t_documentdescription,t_clientname,t_filename,t_status,t_documenttypeid,t_year,t_keyid)
			SELECT CONCAT(v_fy,'-06-30'),'Annual Compliance Documents', CONCAT(v_fy,' ',b.typename),'N/A','',
				CASE LOWER(v_checkliststatus) WHEN 'submitted' THEN 'Complete' ELSE 'Waiting on Checklist' END,b.foreigntypeid,v_ayear,v_fyid
			FROM fund_document_base b 
			WHERE b.foreigntablename='cl_fy_wipreview_files'  AND b.foreigntypeid<>99 AND IFNULL(b.foreigntypeid,0)=17 ORDER BY b.sortorder;
		END IF;
		/*end dwyane add*/
		/*insert into temp_documents_table(t_date,t_documenttype,t_documentdescription,t_clientname,t_filename,t_status,t_documenttypeid,t_year,t_keyid)
		select concat(v_fy,'-06-30'),'Annual Compliance Documents', concat(v_fy,' ',b.typename),'N/A','',
			case lower(v_checkliststatus) when 'submitted' then 'Complete' else 'Waiting on Checklist' end,b.foreigntypeid,v_ayear,v_fyid
		from fund_document_base b 
		where b.foreigntablename='cl_fy_wipreview_files'  and b.foreigntypeid<>99 and ifnull(b.foreigntypeid,0)<9 order by b.sortorder;*/
		
		/*added by fengjie, 12/8/2016, CPV3-448*/
		IF v_isshow=1 THEN
		 INSERT INTO temp_documents_table(t_date,t_documenttype,t_documentdescription,t_clientname,t_filename,t_status,t_documenttypeid,t_year,t_keyid)
		 SELECT CONCAT(v_fy,'-06-30'),'Annual Compliance Documents', CONCAT(v_fy,' ',b.typename),'N/A','',
		  CASE LOWER(v_checkliststatus) WHEN 'submitted' THEN 'Complete' ELSE 'Waiting on Checklist' END,b.foreigntypeid,v_ayear,v_fyid
		 FROM fund_document_base b 
		 WHERE b.foreigntablename='cl_fy_wipreview_files' AND b.description='annual compliance' ORDER BY b.sortorder;
		ELSE
		 INSERT INTO temp_documents_table(t_date,t_documenttype,t_documentdescription,t_clientname,t_filename,t_status,t_documenttypeid,t_year,t_keyid)
		 SELECT CONCAT(v_fy,'-06-30'),'Annual Compliance Documents', CONCAT(v_fy,' ',b.typename),'N/A','',
		  CASE LOWER(v_checkliststatus) WHEN 'submitted' THEN 'Complete' ELSE 'Waiting on Checklist' END,b.foreigntypeid,v_ayear,v_fyid
		 FROM fund_document_base b 
		 WHERE b.foreigntablename='cl_fy_wipreview_files' AND b.description='annual compliance' AND b.foreigntypeid<>15 ORDER BY b.sortorder;
		END IF;
		/*remove the ac*/
		IF v_isneedac=0 THEN
			DELETE FROM temp_documents_table  WHERE t_documenttype='Annual Compliance Documents' AND t_date=CONCAT(v_fy,'-06-30') AND t_documenttypeid=0;
		END IF;
		/*insert all document option: t_documenttypeid:null*/
		INSERT INTO temp_documents_table(t_date,t_documenttype,t_documentdescription,t_clientname,t_filename,t_status,t_documenttypeid,t_year,t_keyid)
		SELECT CONCAT(v_fy,'-06-30'), 'Annual Compliance Documents', CONCAT(v_fy,' All Compliance Documents'),'N/A','all',
			CASE LOWER(v_checkliststatus) WHEN 'submitted' THEN 'Complete' ELSE 'Waiting on Checklist' END,NULL,v_ayear,v_fyid; 
		IF v_lodgeid=1 AND LOWER(v_checkliststatus)='submitted' THEN
			UPDATE temp_documents_table c SET c.t_status='Being Prepared' 
			WHERE c.t_documenttype='Annual Compliance Documents' AND t_date=CONCAT(v_fy,'-06-30');
		END IF;
		IF v_lodgeid=2 AND LOWER(v_checkliststatus)='submitted' THEN
			UPDATE temp_documents_table c SET c.t_status='Ready for Signing' 
			WHERE c.t_documenttype='Annual Compliance Documents' AND t_date=CONCAT(v_fy,'-06-30');
		END IF;
		IF v_lodgeid=3 AND LOWER(v_checkliststatus)='submitted' THEN
			UPDATE temp_documents_table c SET c.t_status='Ready to Lodge' 
			WHERE c.t_documenttype='Annual Compliance Documents' AND t_date=CONCAT(v_fy,'-06-30');
		END IF;
		IF v_lodgeid=6 AND LOWER(v_checkliststatus)='submitted' THEN
			UPDATE temp_documents_table c SET c.t_status='Sent to ATO' 
			WHERE c.t_documenttype='Annual Compliance Documents' AND t_date=CONCAT(v_fy,'-06-30');
		END IF;
		IF v_accountantid=27 THEN 
			UPDATE temp_documents_table c SET c.t_rnr='yes'
			WHERE c.t_documenttype='Annual Compliance Documents' AND t_date=CONCAT(v_fy,'-06-30');
			UPDATE temp_documents_table c SET c.t_status='Complete'
			WHERE c.t_documenttype='Annual Compliance Documents' AND t_date=CONCAT(v_fy,'-06-30') AND t_status='Sent to ATO';
		END IF;
		UPDATE temp_documents_table c, cl_fy_wipreview_files a 
		SET c.t_filename=CASE a.typeid WHEN 7 THEN CONCAT(a.FileName,'!!~/VD_WIPReviewSheets/') WHEN 8 THEN CONCAT(a.FileName,'!!~/VD_WIPReviewSheets/') ELSE CONCAT(a.FileName,'!!~/nnfunds/WIPReviewSheets/') END
		WHERE c.t_keyid =a.fyid AND c.t_documenttypeid=a.typeid  AND c.t_documenttype='Annual Compliance Documents';
		/*--if exists lodged then delete 5,6; if not lodged retains 5 & 6, then delete 7 & 8--*/
		IF v_lodgeid=4 THEN
			DELETE FROM temp_documents_table WHERE t_documenttype='Annual Compliance Documents' AND t_date=CONCAT(v_fy,'-06-30') AND t_documenttypeid IN (5,6);
		ELSE
			DELETE FROM temp_documents_table WHERE t_documenttype='Annual Compliance Documents' AND t_date=CONCAT(v_fy,'-06-30') AND t_documenttypeid IN (7,8);
		END IF; 
		/*delete CGT Cost Base Reset Report*/
		SELECT COUNT(*) INTO v_cgtfilecount FROM cl_fy_wipreview_files WHERE fyid=v_fyid AND typeid=18;
		IF v_cgtfilecount = 0 THEN
			DELETE FROM temp_documents_table WHERE t_documenttype='Annual Compliance Documents' AND t_documenttypeid=18 AND t_keyid =v_fyid;
		END IF;
		
		/*delete Trustee Declaration and Digital Signing for >= 2018. Use SQL Server instead*/
		IF v_ayear >= 2018 THEN			
			DELETE FROM temp_documents_table WHERE t_keyid = v_fyid AND (t_documentdescription LIKE '%Trustee Declaration%' OR t_documentdescription LIKE '%Lodgement Declaration%');
		END IF;
		/**--update the time again*/
		UPDATE temp_documents_table c, cl_fy_wipreview_files a  SET c.t_date=a.CreateTime WHERE c.t_keyid =a.fyid AND c.t_documenttypeid=a.typeid;
	END IF;
    END$$

DELIMITER ;