DELIMITER $$

DROP TRIGGER /*!50032 IF EXISTS */ `tracking_insert_bankaccount`$$

CREATE TRIGGER `tracking_insert_bankaccount` AFTER INSERT ON `fundbankaccount` 
    FOR EACH ROW BEGIN
        INSERT INTO fund_change_tracking(table_name,action_flag,source_id,action_time)
        VALUES('fundbankaccount','I',new.Id,NOW());
END;
$$

DELIMITER ;