DELIMITER $$

USE `esuperfund_main_live`$$

DROP PROCEDURE IF EXISTS `USP_Document_GetAllDocumentsFeeV3`$$

CREATE PROCEDURE `USP_Document_GetAllDocumentsFeeV3`(v_fundid INT,v_year INT,v_type INT,v_status INT)
BEGIN
   DECLARE v_fundstatusID INT DEFAULT 0; 
   DECLARE v_deedupgrade INT DEFAULT 0;
   DECLARE v_fundyear INT DEFAULT 0;
   DECLARE v_fycount INT DEFAULT 0;
   DECLARE v_fyid INT DEFAULT 0;
   DECLARE v_fy INT DEFAULT 0;
   DECLARE v_lodgeid INT DEFAULT 0;
   DECLARE v_isneedac INT DEFAULT 0;
   DECLARE v_checkliststatus VARCHAR(100) DEFAULT '';
   DECLARE v_deleteflag INT DEFAULT 0;
   DECLARE v_issetupcomplete INT DEFAULT 0;
   DECLARE v_accountantid VARCHAR(10) DEFAULT 0;
   DECLARE v_date1 DATETIME;
   DECLARE v_ayear INT DEFAULT 0;
   DECLARE v_compliancereviewcount INT DEFAULT 0; /*added by Dwyane, 6/8/2015, adminend-561*/
   DECLARE v_isshow INT DEFAULT 0;
   DECLARE v_cgtfilecount INT DEFAULT 0;   
   CREATE TEMPORARY TABLE temp_documents_table(
  v_rowid INT AUTO_INCREMENT PRIMARY KEY,
  v_date VARCHAR(30),
  v_documenttype VARCHAR(100),
  v_documentdescription VARCHAR(200),
  v_clientname VARCHAR(100),
  v_filename VARCHAR(200),
  v_documents VARCHAR(100),
  v_status VARCHAR(50),
  v_keyid INT,
  v_documenttypeid INT,
  v_rnr VARCHAR(10) DEFAULT 'no'
   );
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED ;
SELECT FundStatusID,issetupcomplete, EXTRACT(YEAR FROM (CASE LOWER(ExistingOrNewSMSF) WHEN 'new smsf' THEN  (EstabDate+INTERVAL 184 DAY)  ELSE  (Applydate+INTERVAL 184 DAY)  END)),(CASE LOWER(ExistingOrNewSMSF) WHEN 'new smsf' THEN  EstabDate  ELSE ApplyDate  END)
  INTO v_fundstatusID,v_issetupcomplete,v_fundyear,v_date1 FROM fund WHERE id=v_fundid;
SELECT COUNT(*) INTO v_fycount FROM cl_fy_fundfinancialyear WHERE fundid=v_fundid;
SELECT MAX(`year`) INTO v_ayear FROM cl_fy_fundfinancialyear WHERE fundid=v_fundid;
 INSERT INTO temp_documents_table(v_date,v_documenttype,v_documentdescription,v_clientname,v_filename,v_documents,v_status,v_keyid,v_documenttypeid)
SELECT a.ApplyDate,'Death','Death Benefit Agreement',CONCAT(b.FirstName,' ',b.LastName),
CONCAT('/nnfunds/BDADocuments/', CASE c.IsSignedUploaded WHEN 1 THEN c.SignedFileName ELSE c.UnsignedFileName END),
CONCAT('View,',a.id,',',a.fundID,',',a.memberID),d.name,v_fundid,1007 FROM 
bda_application AS a,fundtrustee AS b,fund_mergeupdate AS c,bda_base_status AS d WHERE c.FundID=b.FundID AND c.FundID=a.fundID AND a.memberID=b.ID AND a.bda_mergeupdate_id=c.ID AND a.StatusID=d.id AND (a.StatusID=3 OR a.StatusID=4) AND a.fundID=v_fundid;
 
 IF v_fundstatusID<3 THEN 
/***scenario 1: ISP not returned ****/
 INSERT INTO temp_documents_table(v_date,v_documenttype,v_documentdescription,v_clientname,v_filename,v_documents,v_status,v_keyid,v_documenttypeid)
 SELECT IFNULL(IF(applydate >='2018-06-01' AND applydate <'2018-07-01','2018-07-01',UploadedDate),CONCAT(v_fundyear,'-07-01')),'SMSF Setup','Initial Setup Package','NA',CONCAT('/nnfunds/ispfile/',InitialSetupPackage),'View','Incomplete',v_fundid,0 FROM 
                                      fund WHERE id=v_fundid;  -- and  length(InitialSetupPackage)>0;
                                      /*****from Comsetup****/
INSERT INTO temp_documents_table(v_date,v_documenttype,v_documentdescription,v_clientname,v_filename,v_documents,v_status,v_keyid,v_documenttypeid)
			SELECT mergeupdate.lastupdatetime,c.TypeName,b.TypeName, 'N/A' ,mergeupdate.UnsignedFileName,'View','Complete',mergeupdate.id,1015				
			FROM fund_mergeupdate mergeupdate 
                        INNER JOIN fund_document_base b ON mergeupdate.TrustDeedTypeID = b.ID 
                        INNER JOIN fund_document_base c ON b.ParentTypeID = c.ID 
                        LEFT JOIN ecompanies d ON d.NnFundID = mergeupdate.FundID
			LEFT JOIN loan_securitycustodian_details l ON l.`CompanyACN`=d.`CompanyAcn`
                        WHERE b.ParentTypeID = 1015 AND d.Source=1  AND mergeupdate.`FundID`=v_fundid;
 ELSEIF v_fundstatusID>=3 THEN
/***scenario 2: ISP  returned ****/
 INSERT INTO temp_documents_table(v_date,v_documenttype,v_documentdescription,v_clientname,v_filename,v_documents,v_status,v_keyid,v_documenttypeid)
 SELECT CONCAT(v_fundyear,'-07-01'),'SMSF Setup', b.typename,'NA','','View',
CASE v_issetupcomplete WHEN 1 THEN 'Complete' ELSE'Incomplete' END,
NULL,b.foreigntypeid 
FROM fund_document_base b 
WHERE b.foreigntablename='fund_upload_files'  AND b.foreigntypeid<>99 AND b.foreigntypeid NOT IN (2,12,100,101) ORDER BY b.sortorder;
/*****from Comsetup****/
/*from Company Setup*/
			INSERT INTO temp_documents_table(v_date,v_documenttype,v_documentdescription,v_clientname,v_filename,v_documents,v_status,v_keyid,v_documenttypeid)
			SELECT mergeupdate.lastupdatetime,c.TypeName,b.TypeName, 'N/A' ,mergeupdate.UnsignedFileName,'View','Complete',mergeupdate.id,1015				
			FROM fund_mergeupdate mergeupdate 
                        INNER JOIN fund_document_base b ON mergeupdate.TrustDeedTypeID = b.ID 
                        INNER JOIN fund_document_base c ON b.ParentTypeID = c.ID 
                        LEFT JOIN ecompanies d ON d.`CertificatePath` = mergeupdate.`UnsignedFileName` OR d.`ConsentFormPath` = mergeupdate.`UnsignedFileName` OR d.`ConstitutionPath` = mergeupdate.`UnsignedFileName` OR d.`Form201Path` = mergeupdate.`UnsignedFileName`
                        WHERE b.ParentTypeID = 1015 AND d.Source=1  AND mergeupdate.`FundID`=v_fundid;
                                                                            
/*****===update the temp table========*/
UPDATE temp_documents_table c, (SELECT * FROM (SELECT * FROM fund_upload_files WHERE fundid=v_fundid  ORDER BY typeid,createtime DESC,id DESC) AS temp GROUP BY temp.typeid) AS a
SET c.v_date=a.CreateTime,
c.v_clientname= 'NA',
c.v_filename=CONCAT('/nnfunds/SMSFDocuments/',a.FileName),
c.v_keyid=a.id
WHERE c.v_documenttypeid =a.typeid AND c.v_documenttypeid!=7;
UPDATE temp_documents_table c, (SELECT * FROM (SELECT * FROM fund_upload_files WHERE fundid=v_fundid  ORDER BY typeid,createtime DESC,id DESC) AS temp GROUP BY temp.typeid) AS a
SET c.v_date=a.CreateTime,
c.v_clientname= 'NA',
c.v_filename=CONCAT('/nnfunds/barcodedocs/identificationfiles/',a.FileName),
c.v_keyid=a.id
WHERE c.v_documenttypeid =a.typeid AND c.v_documenttypeid=7;
/* fsp*/
 INSERT INTO temp_documents_table(v_date,v_documenttype,v_documentdescription,v_clientname,v_filename,v_documents,v_status,v_keyid,v_documenttypeid)
 SELECT IFNULL(SetupCompleteDate,v_date1),'SMSF Setup','Final Setup Package','NA',CONCAT('/nnfunds/fspfile/',FinalSetupPackage),'View', 
CASE v_issetupcomplete WHEN 1 THEN 'Complete' ELSE 'Incomplete' END,v_fundid,0 FROM 
                                        fund WHERE id=v_fundid; 
/***scenario 3: add the  trust deed and investment strategy ****/
INSERT INTO temp_documents_table(v_date,v_documenttype,v_documentdescription,v_clientname,v_filename,v_documents,v_status,v_keyid,v_documenttypeid)
SELECT b.LastUpdatetime,
'SMSF Setup',
CASE b.EffectiveRangeID  WHEN 1 THEN 'Trust Deed' 
WHEN 2  THEN  (CASE b.IsDeedUpgrade WHEN 1 THEN'Amended Trust Deed' ELSE 'Trust Deed' END)
WHEN 5  THEN  (CASE b.IsDeedUpgrade WHEN 1 THEN'Amended Trust Deed' ELSE 'Trust Deed' END)
WHEN 3 THEN  'Investment Strategy' 
WHEN 4 THEN  (CASE b.IsDeedUpgrade WHEN 1 THEN 'Amended Investment Strategy' ELSE 'Investment Strategy' END) END,
'NA',
CASE b.IsDeedUpgrade WHEN 1  THEN 
(CASE LENGTH(b.SignedFileName) WHEN 0 THEN CONCAT('/nnfunds/TrustDeedFile/',b.UnSignedFileName) ELSE CONCAT('/nnfunds/TrustDeedFile/',b.SignedFileName) END) 
ELSE
(CASE LENGTH(b.SignedFileName) WHEN 0 THEN '' ELSE CONCAT('/nnfunds/TrustDeedFile/',b.SignedFileName) END) 
END,
'View',
CASE b.IsDeedUpgrade WHEN 1 THEN
(CASE b.issigneduploaded WHEN 1 THEN 'Complete' ELSE 'Incomplete' END) ELSE
(CASE v_issetupcomplete WHEN 1 THEN 'Complete' ELSE 'Incomplete' END) END,
b.id,
CASE b.EffectiveRangeID  WHEN 1 THEN -1 WHEN 2 THEN -2 WHEN 3 THEN -3 WHEN 4 THEN -4 WHEN 5 THEN -5 END
FROM fund_mergeupdate b WHERE fundid=v_fundid AND b.EffectiveRangeID IN(1,2,3,4,5) AND b.trustdeedtypeid<300;
 END IF;
/******scenario 4: rollover received documents******/
 INSERT INTO temp_documents_table(v_date,v_documenttype,v_documentdescription,v_clientname,v_filename,v_documents,v_status,v_keyid,v_documenttypeid)
SELECT  a.CreateTime ,'Rollovers' , b.typename, func_GetTrusteeName(c.fundmemberid),CONCAT('/nnfunds/SMSFDocuments/',a.FileName),'View','Complete',c.id,a.typeid         
FROM fund_upload_files a , fund_document_base b,fund_rollover c WHERE a.typeid =b.foreigntypeid AND a.ForeignID=c.id AND b.foreigntablename='fund_upload_files' AND  a.fundid=v_fundid AND b.foreigntypeid=99;
/******scenario 6: contribution Remittance Advice ******/
 
INSERT INTO temp_documents_table(v_date,v_documenttype,v_documentdescription,v_clientname,v_filename,v_documents,v_status,v_keyid,v_documenttypeid)
SELECT a.CreateTime, 'Contribution', c.description,func_GetTrusteeNameByContributionId(b.id, 'contribution_co_member'),
CONCAT('/nnfunds/SMSFDocuments/',a.FileName),'View','Complete',a.ForeignID,a.typeid         
FROM fund_upload_files a INNER JOIN contribution_co b ON a.foreignid=b.id 
INNER JOIN fund_document_base c ON a.typeid=c.foreigntypeid
WHERE a.fundid=v_fundid AND  a.isDelete=0 AND a.FileName<>'undefined' AND a.typeid=100;
INSERT INTO temp_documents_table(v_date,v_documenttype,v_documentdescription,v_clientname,v_filename,v_documents,v_status,v_keyid,v_documenttypeid)
SELECT a.CreateTime, 'Contribution', c.description,func_GetTrusteeNameByContributionId(b.id, 'contribution_lisc_member'),
CONCAT('/nnfunds/SMSFDocuments/',a.FileName),'View','Complete',a.ForeignID,a.typeid         
FROM fund_upload_files a INNER JOIN contribution_lisc b ON a.foreignid=b.id 
INNER JOIN fund_document_base c ON a.typeid=c.foreigntypeid
WHERE a.fundid=v_fundid AND  a.isDelete=0 AND a.FileName<>'undefined' AND a.typeid=101; 
 
/***scenario 5: Annual Compliance Documents-- loop*/
WHILE v_fycount>0 DO 
SET v_isneedac=0;
SELECT id,`year`,ChecklistStatus,LodgementStatusID,AccountantID,IFNULL(IsPensionNeedAC,0),IFNULL(GSTLodgeRequired,0) INTO v_fyid,v_fy,v_checkliststatus,v_lodgeid,v_accountantid,v_isneedac,v_isshow
FROM cl_fy_fundfinancialyear WHERE fundid=v_fundid AND `year`= v_ayear; -- case v_fycount when 2 then 2011 else 2012 end;
/* added by Dwyane, 6/8/2015, adminend-561*/
SELECT COUNT(*) INTO v_compliancereviewcount FROM wip_fy_reviewsheet WHERE ReviewItemID IN(20, 42) AND fyid=v_fyid AND YesOrNo=1;
IF v_compliancereviewcount=1 THEN
INSERT INTO temp_documents_table(v_date,v_documenttype,v_documentdescription,v_clientname,v_filename,v_documents,v_status,v_keyid,v_documenttypeid)
SELECT CONCAT(v_fy,'-06-30'),'Annual Compliance Documents', CONCAT(v_fy,' ',b.typename),'NA','',
CASE LOWER(v_checkliststatus) WHEN 'submitted' THEN 'View' ELSE 'Waiting for Checklist' END,
CASE LOWER(v_checkliststatus) WHEN 'submitted' THEN 'Complete' ELSE 'Waiting for Checklist' END,
v_fyid,b.foreigntypeid 
FROM fund_document_base b 
WHERE b.foreigntablename='cl_fy_wipreview_files'  AND b.foreigntypeid<>99 AND IFNULL(b.foreigntypeid,0)=17 ORDER BY b.sortorder;
END IF;
/*end dwyane add*/
/*INSERT INTO temp_documents_table(v_date,v_documenttype,v_documentdescription,v_clientname,v_filename,v_documents,v_status,v_keyid,v_documenttypeid)
SELECT CONCAT(v_fy,'-06-30'),'Annual Compliance Documents', CONCAT(v_fy,' ',b.typename),'NA','',
CASE LOWER(v_checkliststatus) WHEN 'submitted' THEN 'View' ELSE 'Waiting for Checklist' END,
CASE LOWER(v_checkliststatus) WHEN 'submitted' THEN 'Complete' ELSE 'Waiting for Checklist' END,
v_fyid,b.foreigntypeid 
FROM fund_document_base b 
WHERE b.foreigntablename='cl_fy_wipreview_files'  AND b.foreigntypeid<>99 AND IFNULL(b.foreigntypeid,0)<9 ORDER BY b.sortorder;*/
/*added by fengjie, 12/8/2016, CPV3-448*/
IF v_isshow=1 THEN
INSERT INTO temp_documents_table(v_date,v_documenttype,v_documentdescription,v_clientname,v_filename,v_documents,v_status,v_keyid,v_documenttypeid)
SELECT CONCAT(v_fy,'-06-30'),'Annual Compliance Documents', CONCAT(v_fy,' ',b.typename),'NA','',
CASE LOWER(v_checkliststatus) WHEN 'submitted' THEN 'View' ELSE 'Waiting for Checklist' END,
CASE LOWER(v_checkliststatus) WHEN 'submitted' THEN 'Complete' ELSE 'Waiting for Checklist' END,
v_fyid,b.foreigntypeid 
FROM fund_document_base b 
WHERE b.foreigntablename='cl_fy_wipreview_files' AND b.description='annual compliance' ORDER BY b.sortorder;
ELSE
INSERT INTO temp_documents_table(v_date,v_documenttype,v_documentdescription,v_clientname,v_filename,v_documents,v_status,v_keyid,v_documenttypeid)
SELECT CONCAT(v_fy,'-06-30'),'Annual Compliance Documents', CONCAT(v_fy,' ',b.typename),'NA','',
CASE LOWER(v_checkliststatus) WHEN 'submitted' THEN 'View' ELSE 'Waiting for Checklist' END,
CASE LOWER(v_checkliststatus) WHEN 'submitted' THEN 'Complete' ELSE 'Waiting for Checklist' END,
v_fyid,b.foreigntypeid 
FROM fund_document_base b 
WHERE b.foreigntablename='cl_fy_wipreview_files' AND b.description='annual compliance' AND b.foreigntypeid<>15 ORDER BY b.sortorder;
END IF;
/*----remove the ac ----*/
IF v_isneedac=0 THEN
DELETE FROM temp_documents_table  WHERE v_documenttype='Annual Compliance Documents' AND v_date=CONCAT(v_fy,'-06-30') AND v_documenttypeid=0;
END IF;
/*---insert all document option*/
INSERT INTO temp_documents_table(v_date,v_documenttype,v_documentdescription,v_clientname,v_filename,v_documents,v_status,v_keyid,v_documenttypeid)
SELECT CONCAT(v_fy,'-06-30'), 'Annual Compliance Documents', CONCAT(v_fy,' All Compliance Documents'),'NA','all',
CASE LOWER(v_checkliststatus) WHEN 'submitted' THEN 'View' ELSE 'Waiting for Checklist' END,
CASE LOWER(v_checkliststatus) WHEN 'submitted' THEN 'Complete' ELSE 'Waiting for Checklist' END,v_fyid,NULL; 
/*========================= Annual Accountant Documents --- section added by lucio 2013-08-14 =================== */ 
INSERT INTO temp_documents_table(v_date,v_documenttype,v_documentdescription,v_clientname,v_filename,v_documents,v_status,v_keyid,v_documenttypeid)
SELECT CONCAT(v_fy,'-06-30'),'Annual Accountant Documents', CONCAT(v_fy,' ',b.typename),'NA','',
CASE LOWER(v_checkliststatus) WHEN 'submitted' THEN 'View' ELSE 'Waiting for Checklist' END,
CASE LOWER(v_checkliststatus) WHEN 'submitted' THEN 'Complete' ELSE 'Waiting for Checklist' END,
v_fyid,b.foreigntypeid 
FROM fund_document_base b 
WHERE b.foreigntablename='cl_fy_wipreview_files'  AND b.foreigntypeid<>99 AND IFNULL(b.foreigntypeid,0)IN (9,10,11) ORDER BY b.sortorder;
 
 ###  checklist files
 /**********************type 22: ATO********************/
INSERT INTO temp_documents_table(v_date,v_documenttype,v_documentdescription,v_clientname,v_filename,v_documents,v_status,v_keyid,v_documenttypeid)
SELECT 	b.createtime,'Annual Accountant Documents', CONCAT(v_fy,' ATO',' - ',func_getSplitString(func_getSplitString(func_getSplitString(b.FileName,'?',1),'.',1),'_',func_getSplitCount(b.FileName,'_'))),'NA',CONCAT('/checklistdocument/',b.FileName),
CASE LOWER(v_checkliststatus) WHEN 'submitted' THEN 'View' ELSE 'Waiting for Checklist' END,
CASE LOWER(v_checkliststatus) WHEN 'submitted' THEN 'Complete' ELSE 'Waiting for Checklist' END,
v_fyid,22 
FROM cl_fy_upload_ato a
LEFT JOIN cl_fy_upload_files b
ON a.ID= b.ForeignID
WHERE b.TableID=22 AND a.DocumentProvideID=1 AND IFNULL(b.FileName,'')<>'' AND a.FYID=v_fyid;
/******************type 23: Accountant Review sheet***********************/
INSERT INTO temp_documents_table(v_date,v_documenttype,v_documentdescription,v_clientname,v_filename,v_documents,v_status,v_keyid,v_documenttypeid)
SELECT 	b.createtime,'Annual Accountant Documents', CONCAT(v_fy,' Accountant Worksheet',' - ',func_getSplitString(func_getSplitString(func_getSplitString(b.FileName,'?',1),'.',1),'_',func_getSplitCount(b.FileName,'_'))),'NA',CONCAT('/checklistdocument/',b.FileName),
CASE LOWER(v_checkliststatus) WHEN 'submitted' THEN 'View' ELSE 'Waiting for Checklist' END,
CASE LOWER(v_checkliststatus) WHEN 'submitted' THEN 'Complete' ELSE 'Waiting for Checklist' END,
v_fyid,23
FROM cl_fy_upload_accountantwt a
LEFT JOIN cl_fy_upload_files b
ON a.ID= b.ForeignID
WHERE b.TableID=23 AND a.DocumentProvideID=1 AND IFNULL(b.FileName,'')<>'' AND a.FYID=v_fyid;
/***----type 1: Rollovers****/
INSERT INTO temp_documents_table(v_date,v_documenttype,v_documentdescription,v_clientname,v_filename,v_documents,v_status,v_keyid,v_documenttypeid)
SELECT 	b.createtime,'Annual Accountant Documents', CONCAT(v_fy,' Rollovers'),'NA',CONCAT('/checklistdocument/',b.FileName),
CASE LOWER(v_checkliststatus) WHEN 'submitted' THEN 'View' ELSE 'Waiting for Checklist' END,
CASE LOWER(v_checkliststatus) WHEN 'submitted' THEN 'Complete' ELSE 'Waiting for Checklist' END,
v_fyid,100
FROM cl_fy_upload_rollovers a
LEFT JOIN cl_fy_upload_files b
ON a.ID= b.ForeignID
WHERE b.TableID=1 AND a.DocumentProvideID=1 AND IFNULL(b.FileName,'')<>'' AND a.FYID=v_fyid;
/***----type 2: Term Deposit****/
INSERT INTO temp_documents_table(v_date,v_documenttype,v_documentdescription,v_clientname,v_filename,v_documents,v_status,v_keyid,v_documenttypeid)
SELECT 	b.createtime,'Annual Accountant Documents', CONCAT(v_fy,' Term Deposits'),'NA',CONCAT('/checklistdocument/',b.FileName),
CASE LOWER(v_checkliststatus) WHEN 'submitted' THEN 'View' ELSE 'Waiting for Checklist' END,
CASE LOWER(v_checkliststatus) WHEN 'submitted' THEN 'Complete' ELSE 'Waiting for Checklist' END,
v_fyid,200
FROM cl_fy_upload_termdeposit a
LEFT JOIN cl_fy_upload_files b
ON a.ID= b.ForeignID
WHERE b.TableID=2 AND a.DocumentProvideID=1 AND IFNULL(b.FileName,'')<>'' AND a.FYID=v_fyid;
 /***----type 3: Other bank account****/
INSERT INTO temp_documents_table(v_date,v_documenttype,v_documentdescription,v_clientname,v_filename,v_documents,v_status,v_keyid,v_documenttypeid)
SELECT 	b.createtime,'Annual Accountant Documents', CONCAT(v_fy,' Other Bank Account'),'NA',CONCAT('/checklistdocument/',b.FileName),
CASE LOWER(v_checkliststatus) WHEN 'submitted' THEN 'View' ELSE 'Waiting for Checklist' END,
CASE LOWER(v_checkliststatus) WHEN 'submitted' THEN 'Complete' ELSE 'Waiting for Checklist' END,
v_fyid,300
FROM cl_fy_upload_otherbankaccount a
LEFT JOIN cl_fy_upload_files b
ON a.ID= b.ForeignID
WHERE b.TableID=3 AND a.DocumentProvideID=1 AND IFNULL(b.FileName,'')<>'' AND a.FYID=v_fyid;
/***********type 4 : asset purchase*************/
INSERT INTO temp_documents_table(v_date,v_documenttype,v_documentdescription,v_clientname,v_filename,v_documents,v_status,v_keyid,v_documenttypeid)
SELECT 	b.createtime,'Annual Accountant Documents', CONCAT(v_fy,' Asset Purchases'),'NA',CONCAT('/checklistdocument/',b.FileName),
CASE LOWER(v_checkliststatus) WHEN 'submitted' THEN 'View' ELSE 'Waiting for Checklist' END,
CASE LOWER(v_checkliststatus) WHEN 'submitted' THEN 'Complete' ELSE 'Waiting for Checklist' END,
v_fyid,400
FROM cl_fy_upload_assetpurchases a
LEFT JOIN cl_fy_upload_files b
ON a.ID= b.ForeignID
WHERE b.TableID=4 AND a.DocumentProvidID=1 AND IFNULL(b.FileName,'')<>'' AND a.FYID=v_fyid;
/***************type 5 : asset sales******************************/
INSERT INTO temp_documents_table(v_date,v_documenttype,v_documentdescription,v_clientname,v_filename,v_documents,v_status,v_keyid,v_documenttypeid)
SELECT 	b.createtime,'Annual Accountant Documents', CONCAT(v_fy,' Asset Sales'),'NA',CONCAT('/checklistdocument/',b.FileName),
CASE LOWER(v_checkliststatus) WHEN 'submitted' THEN 'View' ELSE 'Waiting for Checklist' END,
CASE LOWER(v_checkliststatus) WHEN 'submitted' THEN 'Complete' ELSE 'Waiting for Checklist' END,
v_fyid,500
FROM cl_fy_upload_assetsales a
LEFT JOIN cl_fy_upload_files b
ON a.ID= b.ForeignID
WHERE b.TableID=5 AND a.DocumentProvidID=1 AND IFNULL(b.FileName,'')<>'' AND a.FYID=v_fyid;
/*****************type 6 : other asset at june 30**********************/
INSERT INTO temp_documents_table(v_date,v_documenttype,v_documentdescription,v_clientname,v_filename,v_documents,v_status,v_keyid,v_documenttypeid)
SELECT 	b.createtime,'Annual Accountant Documents', CONCAT(v_fy,' Other Assets at 30 June'),'NA',CONCAT('/checklistdocument/',b.FileName),
CASE LOWER(v_checkliststatus) WHEN 'submitted' THEN 'View' ELSE 'Waiting for Checklist' END,
CASE LOWER(v_checkliststatus) WHEN 'submitted' THEN 'Complete' ELSE 'Waiting for Checklist' END,
v_fyid,600
FROM cl_fy_upload_otherassetsat30june a
LEFT JOIN cl_fy_upload_files b
ON a.ID= b.ForeignID
WHERE b.TableID=6 AND a.DocumentProvidID=1 AND IFNULL(b.FileName,'')<>'' AND a.FYID=v_fyid;
/**************type 7 : dividends******************************/
INSERT INTO temp_documents_table(v_date,v_documenttype,v_documentdescription,v_clientname,v_filename,v_documents,v_status,v_keyid,v_documenttypeid)
SELECT 	b.createtime,'Annual Accountant Documents', CONCAT(v_fy,' Dividends'),'NA',CONCAT('/checklistdocument/',b.FileName),
CASE LOWER(v_checkliststatus) WHEN 'submitted' THEN 'View' ELSE 'Waiting for Checklist' END,
CASE LOWER(v_checkliststatus) WHEN 'submitted' THEN 'Complete' ELSE 'Waiting for Checklist' END,
v_fyid,700
FROM cl_fy_upload_dividends a
LEFT JOIN cl_fy_upload_files b
ON a.ID= b.ForeignID
WHERE b.TableID=7 AND a.DocumentProvideID=1 AND IFNULL(b.FileName,'')<>'' AND a.FYID=v_fyid;
/*******************type 8 : dividends reinvestment********************************/
INSERT INTO temp_documents_table(v_date,v_documenttype,v_documentdescription,v_clientname,v_filename,v_documents,v_status,v_keyid,v_documenttypeid)
SELECT 	b.createtime,'Annual Accountant Documents', CONCAT(v_fy,' Dividend Reinvestments'),'NA',CONCAT('/checklistdocument/',b.FileName),
CASE LOWER(v_checkliststatus) WHEN 'submitted' THEN 'View' ELSE 'Waiting for Checklist' END,
CASE LOWER(v_checkliststatus) WHEN 'submitted' THEN 'Complete' ELSE 'Waiting for Checklist' END,
v_fyid,800
FROM cl_fy_upload_dividendreinvestments a
LEFT JOIN cl_fy_upload_files b
ON a.ID= b.ForeignID
WHERE b.TableID=8 AND a.DocumentProvideID=1 AND IFNULL(b.FileName,'')<>'' AND a.FYID=v_fyid;
/****************type 9 : distribution reinvestments*************************/
INSERT INTO temp_documents_table(v_date,v_documenttype,v_documentdescription,v_clientname,v_filename,v_documents,v_status,v_keyid,v_documenttypeid)
SELECT 	b.createtime,'Annual Accountant Documents', CONCAT(v_fy,' Distribution Reinvestments'),'NA',CONCAT('/checklistdocument/',b.FileName),
CASE LOWER(v_checkliststatus) WHEN 'submitted' THEN 'View' ELSE 'Waiting for Checklist' END,
CASE LOWER(v_checkliststatus) WHEN 'submitted' THEN 'Complete' ELSE 'Waiting for Checklist' END,
v_fyid,900
FROM cl_fy_upload_distributionreinvestments a
LEFT JOIN cl_fy_upload_files b
ON a.ID= b.ForeignID
WHERE b.TableID=9 AND a.DocumentProvidID=1 AND IFNULL(b.FileName,'')<>'' AND a.FYID=v_fyid;
/**********************type 10 : tax dectuion notice******************************************************/
INSERT INTO temp_documents_table(v_date,v_documenttype,v_documentdescription,v_clientname,v_filename,v_documents,v_status,v_keyid,v_documenttypeid)
SELECT 	b.createtime,'Annual Accountant Documents', CONCAT(v_fy,' Tax Deduction Notice'),'NA',CONCAT('/checklistdocument/',b.FileName),
CASE LOWER(v_checkliststatus) WHEN 'submitted' THEN 'View' ELSE 'Waiting for Checklist' END,
CASE LOWER(v_checkliststatus) WHEN 'submitted' THEN 'Complete' ELSE 'Waiting for Checklist' END,
v_fyid,900
FROM cl_fy_upload_taxdeductionnotice a
LEFT JOIN cl_fy_upload_files b
ON a.ID= b.ForeignID
WHERE b.TableID=10 AND a.DocumentProvidID=1 AND IFNULL(b.FileName,'')<>'' AND a.FYID=v_fyid;
/*****************************type 11 : off market transfer *************************************************************************/
INSERT INTO temp_documents_table(v_date,v_documenttype,v_documentdescription,v_clientname,v_filename,v_documents,v_status,v_keyid,v_documenttypeid)
SELECT 	b.createtime,'Annual Accountant Documents', CONCAT(v_fy,' Off Market Transfers'),'NA',CONCAT('/checklistdocument/',b.FileName),
CASE LOWER(v_checkliststatus) WHEN 'submitted' THEN 'View' ELSE 'Waiting for Checklist' END,
CASE LOWER(v_checkliststatus) WHEN 'submitted' THEN 'Complete' ELSE 'Waiting for Checklist' END,
v_fyid,900
FROM cl_fy_upload_offmarkettransfers a
LEFT JOIN cl_fy_upload_files b
ON a.ID= b.ForeignID
WHERE b.TableID=11 AND a.DocumentProvideID=1 AND IFNULL(b.FileName,'')<>'' AND a.FYID=v_fyid;
/************************type 12 : annual tax status*************************************/
INSERT INTO temp_documents_table(v_date,v_documenttype,v_documentdescription,v_clientname,v_filename,v_documents,v_status,v_keyid,v_documenttypeid)
SELECT 	b.createtime,'Annual Accountant Documents', CONCAT(v_fy,' Annual Tax Statements'),'NA',CONCAT('/checklistdocument/',b.FileName),
CASE LOWER(v_checkliststatus) WHEN 'submitted' THEN 'View' ELSE 'Waiting for Checklist' END,
CASE LOWER(v_checkliststatus) WHEN 'submitted' THEN 'Complete' ELSE 'Waiting for Checklist' END,
v_fyid,900
FROM cl_fy_upload_annualtaxstatements a
LEFT JOIN cl_fy_upload_files b
ON a.ID= b.ForeignID
WHERE b.TableID=12 AND a.DocumentProvideID=1 AND IFNULL(b.FileName,'')<>'' AND a.FYID=v_fyid;
/*************************type 13 : Corporate actions****************************************************/
INSERT INTO temp_documents_table(v_date,v_documenttype,v_documentdescription,v_clientname,v_filename,v_documents,v_status,v_keyid,v_documenttypeid)
SELECT 	b.createtime,'Annual Accountant Documents', CONCAT(v_fy,' Corporate Actions'),'NA',CONCAT('/checklistdocument/',b.FileName),
CASE LOWER(v_checkliststatus) WHEN 'submitted' THEN 'View' ELSE 'Waiting for Checklist' END,
CASE LOWER(v_checkliststatus) WHEN 'submitted' THEN 'Complete' ELSE 'Waiting for Checklist' END,
v_fyid,900
FROM cl_fy_upload_corporateactions a
LEFT JOIN cl_fy_upload_files b
ON a.ID= b.ForeignID
WHERE b.TableID=13 AND a.DocumentProvidID=1 AND IFNULL(b.FileName,'')<>'' AND a.FYID=v_fyid;
/********************************type 14 : Contribution split**********************************************************************/
INSERT INTO temp_documents_table(v_date,v_documenttype,v_documentdescription,v_clientname,v_filename,v_documents,v_status,v_keyid,v_documenttypeid)
SELECT 	b.createtime,'Annual Accountant Documents', CONCAT(v_fy,' Contributions Splitting'),'NA',CONCAT('/checklistdocument/',b.FileName),
CASE LOWER(v_checkliststatus) WHEN 'submitted' THEN 'View' ELSE 'Waiting for Checklist' END,
CASE LOWER(v_checkliststatus) WHEN 'submitted' THEN 'Complete' ELSE 'Waiting for Checklist' END,
v_fyid,900
FROM cl_fy_upload_contributionssplitting a
LEFT JOIN cl_fy_upload_files b
ON a.ID= b.ForeignID
WHERE b.TableID=14 AND a.DocumentProvideID=1 AND IFNULL(b.FileName,'')<>'' AND a.FYID=v_fyid;
/***************************type 15 : property loan*************************************/
INSERT INTO temp_documents_table(v_date,v_documenttype,v_documentdescription,v_clientname,v_filename,v_documents,v_status,v_keyid,v_documenttypeid)
SELECT 	b.createtime,'Annual Accountant Documents', CONCAT(v_fy,' Property'),'NA',CONCAT('/checklistdocument/',b.FileName),
CASE LOWER(v_checkliststatus) WHEN 'submitted' THEN 'View' ELSE 'Waiting for Checklist' END,
CASE LOWER(v_checkliststatus) WHEN 'submitted' THEN 'Complete' ELSE 'Waiting for Checklist' END,
v_fyid,900
FROM cl_fy_upload_propertyloan a
LEFT JOIN cl_fy_upload_files b
ON a.ID= b.ForeignID
WHERE b.TableID=15 AND a.DocumentProvidID=1 AND IFNULL(b.FileName,'')<>'' AND a.FYID=v_fyid;
/***************************type 16 : Insurance policies************************************/
INSERT INTO temp_documents_table(v_date,v_documenttype,v_documentdescription,v_clientname,v_filename,v_documents,v_status,v_keyid,v_documenttypeid)
SELECT 	b.createtime,'Annual Accountant Documents', CONCAT(v_fy,' Insurance'),'NA',CONCAT('/checklistdocument/',b.FileName),
CASE LOWER(v_checkliststatus) WHEN 'submitted' THEN 'View' ELSE 'Waiting for Checklist' END,
CASE LOWER(v_checkliststatus) WHEN 'submitted' THEN 'Complete' ELSE 'Waiting for Checklist' END,
v_fyid,900
FROM cl_fy_upload_insurancepolicies a
LEFT JOIN cl_fy_upload_files b
ON a.ID= b.ForeignID
WHERE b.TableID=16 AND a.DocumentProvidID=1 AND IFNULL(b.FileName,'')<>'' AND a.FYID=v_fyid;
/*********************type 17 : other outflows***********************/
INSERT INTO temp_documents_table(v_date,v_documenttype,v_documentdescription,v_clientname,v_filename,v_documents,v_status,v_keyid,v_documenttypeid)
SELECT 	b.createtime,'Annual Accountant Documents', CONCAT(v_fy,' Other Outflows'),'NA',CONCAT('/checklistdocument/',b.FileName),
CASE LOWER(v_checkliststatus) WHEN 'submitted' THEN 'View' ELSE 'Waiting for Checklist' END,
CASE LOWER(v_checkliststatus) WHEN 'submitted' THEN 'Complete' ELSE 'Waiting for Checklist' END,
v_fyid,900
FROM cl_fy_upload_otheroutflows a
LEFT JOIN cl_fy_upload_files b
ON a.ID= b.ForeignID
WHERE b.TableID=17 AND a.DocumentProvideID=1 AND IFNULL(b.FileName,'')<>'' AND a.FYID=v_fyid;
/********************type 18 : other inflows*****************************/
INSERT INTO temp_documents_table(v_date,v_documenttype,v_documentdescription,v_clientname,v_filename,v_documents,v_status,v_keyid,v_documenttypeid)
SELECT 	b.createtime,'Annual Accountant Documents', CONCAT(v_fy,' Other Inflows'),'NA',CONCAT('/checklistdocument/',b.FileName),
CASE LOWER(v_checkliststatus) WHEN 'submitted' THEN 'View' ELSE 'Waiting for Checklist' END,
CASE LOWER(v_checkliststatus) WHEN 'submitted' THEN 'Complete' ELSE 'Waiting for Checklist' END,
v_fyid,900
FROM cl_fy_upload_otherInflows a
LEFT JOIN cl_fy_upload_files b
ON a.ID= b.ForeignID
WHERE b.TableID=18 AND a.DocumentProvideID=1 AND IFNULL(b.FileName,'')<>'' AND a.FYID=v_fyid;
/***********************type 19 : other uploads***************************************/
INSERT INTO temp_documents_table(v_date,v_documenttype,v_documentdescription,v_clientname,v_filename,v_documents,v_status,v_keyid,v_documenttypeid)
SELECT 	b.createtime,'Annual Accountant Documents', CONCAT(v_fy,' Other Documentation'),'NA',CONCAT('/checklistdocument/',b.FileName),
CASE LOWER(v_checkliststatus) WHEN 'submitted' THEN 'View' ELSE 'Waiting for Checklist' END,
CASE LOWER(v_checkliststatus) WHEN 'submitted' THEN 'Complete' ELSE 'Waiting for Checklist' END,
v_fyid,900
FROM cl_fy_upload_otheruploads a
LEFT JOIN cl_fy_upload_files b
ON a.ID= b.ForeignID
WHERE b.TableID=19 AND a.DocumentProvideID=1 AND IFNULL(b.FileName,'')<>'' AND a.FYID=v_fyid;
/************************type 20 : retirement declarations****************************/
INSERT INTO temp_documents_table(v_date,v_documenttype,v_documentdescription,v_clientname,v_filename,v_documents,v_status,v_keyid,v_documenttypeid)
SELECT 	b.createtime,'Annual Accountant Documents', CONCAT(v_fy,' Retirement Declaration'),'NA',CONCAT('/checklistdocument/',b.FileName),
CASE LOWER(v_checkliststatus) WHEN 'submitted' THEN 'View' ELSE 'Waiting for Checklist' END,
CASE LOWER(v_checkliststatus) WHEN 'submitted' THEN 'Complete' ELSE 'Waiting for Checklist' END,
v_fyid,900
FROM cl_fy_upload_retirementdeclarations a
LEFT JOIN cl_fy_upload_files b
ON a.ID= b.ForeignID
WHERE b.TableID=20 AND a.DocumentProvideID=1 AND IFNULL(b.FileName,'')<>'' AND a.FYID=v_fyid;
/************type 21: Excess contribution tax*********************************/
INSERT INTO temp_documents_table(v_date,v_documenttype,v_documentdescription,v_clientname,v_filename,v_documents,v_status,v_keyid,v_documenttypeid)
SELECT 	b.createtime,'Annual Accountant Documents', CONCAT(v_fy,' Excess Contribution Tax '),'NA',CONCAT('/checklistdocument/',b.FileName),
CASE LOWER(v_checkliststatus) WHEN 'submitted' THEN 'View' ELSE 'Waiting for Checklist' END,
CASE LOWER(v_checkliststatus) WHEN 'submitted' THEN 'Complete' ELSE 'Waiting for Checklist' END,
v_fyid,900
FROM cl_fy_upload_excesscontributiontax a
LEFT JOIN cl_fy_upload_files b
ON a.ID= b.ForeignID
WHERE b.TableID=21 AND a.DocumentProvideID=1 AND IFNULL(b.FileName,'')<>'' AND a.FYID=v_fyid;
/*========================= Annual Accountant Documents --- section added by lucio 2013-08-14 end=================== */ 
IF v_lodgeid=1 AND LOWER(v_checkliststatus)='submitted' THEN
UPDATE temp_documents_table c SET c.v_documents='Being Prepared',c.v_status='Being Prepared' 
WHERE c.v_documenttype='Annual Compliance Documents' AND v_date=CONCAT(v_fy,'-06-30');
END IF;
IF v_lodgeid=2 AND LOWER(v_checkliststatus)='submitted' THEN
UPDATE temp_documents_table c SET c.v_documents='View',c.v_status='Ready for Signing' 
WHERE c.v_documenttype='Annual Compliance Documents' AND v_date=CONCAT(v_fy,'-06-30');
END IF;
IF v_lodgeid=3 AND LOWER(v_checkliststatus)='submitted' THEN
UPDATE temp_documents_table c SET c.v_documents='View',c.v_status='Ready to Lodge' 
WHERE c.v_documenttype='Annual Compliance Documents' AND v_date=CONCAT(v_fy,'-06-30');
END IF;
IF v_lodgeid=6 AND LOWER(v_checkliststatus)='submitted' THEN
UPDATE temp_documents_table c SET c.v_documents='View',c.v_status='Sent to ATO' 
WHERE c.v_documenttype='Annual Compliance Documents' AND v_date=CONCAT(v_fy,'-06-30');
END IF;
IF v_accountantid=27 THEN 
UPDATE temp_documents_table c SET c.v_rnr='yes'
WHERE c.v_documenttype='Annual Compliance Documents' AND v_date=CONCAT(v_fy,'-06-30');
END IF;
/*****===update the temp table========*/
UPDATE temp_documents_table c, cl_fy_wipreview_files a 
SET -- c.v_date=a.CreateTime,
c.v_filename=CONCAT('/nnfunds/WIPReviewSheets/',a.FileName)
WHERE c.v_keyid =a.fyid AND c.v_documenttypeid=a.typeid  AND c.v_documenttype='Annual Compliance Documents';
/*****===update the temp table - Annual Accountant Documents========*/
UPDATE temp_documents_table c, cl_fy_wipreview_files a 
SET -- c.v_date=a.CreateTime,
c.v_filename=CONCAT('/nnfunds/WIPReviewSheets/',a.FileName)
WHERE c.v_keyid =a.fyid AND c.v_documenttypeid=a.typeid AND a.typeid IN(9,10,11)  AND c.v_documenttype='Annual Accountant Documents';
/*--if exists lodged then delete 5,6; if not lodged retains 5 & 6, then delete 7 & 8--*/
IF v_lodgeid=4 THEN
DELETE FROM temp_documents_table WHERE v_documenttype='Annual Compliance Documents' AND v_date=CONCAT(v_fy,'-06-30') AND v_documenttypeid IN (5,6);
ELSE
DELETE FROM temp_documents_table WHERE v_documenttype='Annual Compliance Documents' AND v_date=CONCAT(v_fy,'-06-30') AND v_documenttypeid IN (7,8);
END IF; 
/*delete CGT Cost Base Reset Report*/
SELECT COUNT(*) INTO v_cgtfilecount FROM cl_fy_wipreview_files WHERE fyid=v_fyid AND typeid=18;
IF v_cgtfilecount = 0 THEN
	DELETE FROM temp_documents_table WHERE v_documenttype='Annual Compliance Documents' AND v_documenttypeid=18 AND v_keyid =v_fyid;
END IF;
/**--update the time again*/
UPDATE temp_documents_table c, cl_fy_wipreview_files a 
SET c.v_date=a.CreateTime
WHERE c.v_keyid =a.fyid AND c.v_documenttypeid=a.typeid;
SET v_fycount=v_fycount-1;
SET v_ayear=v_ayear-1;
END WHILE;
UPDATE temp_documents_table SET v_documenttypeid=1002 WHERE v_documenttype='Annual Compliance Documents';
UPDATE temp_documents_table SET v_documenttypeid=1011 WHERE v_documenttype='Annual Accountant Documents';
/***scenario 6: Fees related ******/
-- Use Fee in SQL Server
/*INSERT INTO temp_documents_table(v_date,v_documenttype,v_documentdescription,v_clientname,v_filename,v_documents,v_status,v_keyid,v_documenttypeid)
 SELECT (CASE a.FeeTypeID WHEN 9 THEN (SELECT EstabDate FROM fund f WHERE f.id=v_fundid) ELSE duedate END) AS duedate,'Fees',CONCAT(CASE a.FeeTypeID WHEN 1 THEN 'Annual Compliance Fees' WHEN 2 THEN 'Trust Deed Update' WHEN 3 THEN 'Actuarial Fee' 
WHEN 4 THEN 'Exit Fee' WHEN 5 THEN 'Loan Application Fee' WHEN 6 THEN 'Other Fees' WHEN 7 THEN 'Company Setup Fee' WHEN 8 THEN 'Bare Trust / Security Custodian Deed Fee' WHEN 9 THEN 'SMSF Setup' END,IF(a.FeeTypeID<>9,' - ',""), IF(a.FeeTypeID<>9,a.year,"")),'NA','',
CONCAT('View','!',CASE a.FeeTypeID WHEN 1 THEN 'Compliance Fee' WHEN 2 THEN 'Trust Deed Update' WHEN 3 THEN 'Actuarial Fees' 
WHEN 4 THEN 'Exit Fees' WHEN 5 THEN 'Loan Application Fees' WHEN 6 THEN 'Other Fees' WHEN 7 THEN 'Company Setup Fees' WHEN 8 THEN 'Bare Trust / Security Custodian Deed Fee' WHEN 9 THEN 'SMSF Setup' END,'!',a.id),
IF(v_fundstatusID = 4,'Complete','Incomplete'),`year`,1004
 FROM admin_fund_compliancefees a WHERE a.fundid=v_fundid;*/
/***scenario 6: Change Bank related ******/
INSERT INTO temp_documents_table(v_date,v_documenttype,v_documentdescription,v_clientname,v_filename,v_documents,v_status,v_keyid,v_documenttypeid)
SELECT b.LastUpdatetime,
'Applications',
CASE b.trustdeedtypeid  WHEN 302 THEN 'CBA ACA Application' 
ELSE 'ANZ V2 Plus Application' END,
'NA',
CASE b.issigneduploaded WHEN 1  THEN 
(CASE LENGTH(b.SignedFileName) WHEN 0 THEN CONCAT('/nnfunds/ChangeBankFile/',b.UnSignedFileName) ELSE CONCAT('/nnfunds/ChangeBankFile/',b.SignedFileName) END) 
ELSE
(CASE LENGTH(b.SignedFileName) WHEN 0 THEN '' ELSE CONCAT('/nnfunds/ChangeBankFile/',b.UnSignedFileName) END) 
END,
'View',
CASE b.issigneduploaded WHEN 1 THEN 'Complete' ELSE 'Incomplete' END,
CASE b.trustdeedtypeid WHEN 301 THEN 1 ELSE 5 END,
1003
FROM fund_mergeupdate b,changebank_history c WHERE b.id=c.fund_mergeupdate_id AND IFNULL(c.IsExpired,0)=0 AND b.fundid=v_fundid AND b.trustdeedtypeid IN (301,302);
/***CommSec Options & CommSec Warrants ******/
/* INSERT INTO temp_documents_table(v_date,v_documenttype,v_documentdescription,v_clientname,v_filename,v_documents,v_status,v_keyid,v_documenttypeid)
SELECT d.LastUpdatetime,'Applications',
	Case c.Application_TypeID
		WHEN 7 THEN 'CommSec Options'
		ELSE 'CommSec Warrants'
	END, 'N/A',
	CONCAT( 
		Case c.Application_TypeID
		WHEN 7 THEN '/nnfunds/CommSecOptions/file/'
		ELSE '/nnfunds/CommSecWarrants/file/'
		END, d.UnsignedFileName),
	'View', 'Complete', 0, 1003
FROM
	broker_management a
		JOIN
	fundbrokeraccount b ON a.FundBrokerAccountID = b.id
		JOIN
	application_history c ON a.applicationid = c.id
		JOIN
	fund_mergeupdate d ON a.FundMergeUpdateID = d.id
WHERE
		c.Application_TypeID in (7, 8)
		AND a.StatusID != 10
		AND IFNULL(a.isdeleted, 0) = 0
		AND IFNULL(c.isdeleted, 0) = 0
		AND c.fundid = d.fundid
		AND c.fundid = v_fundid
		AND a.statusid IS NOT NULL
ORDER BY d.LastUpdatetime DESC;*/
/***scenario 7:  Annual Accountant Documents ******/
/** show all document**/
SELECT (CASE v_documenttype WHEN 'Annual Compliance Documents' THEN SUBSTRING(v_documentdescription,1,4) WHEN 'Annual Accountant Documents' THEN SUBSTRING(v_documentdescription,1,4)
WHEN 'Fees' THEN v_keyid
ELSE  EXTRACT(YEAR FROM (v_date+INTERVAL 184 DAY)) END) AS Fyear,v_lodgeid,v_isneedac,
a.* FROM temp_documents_table a; -- order by a.v_date desc;
DROP TABLE temp_documents_table;
COMMIT;
    END$$

DELIMITER ;