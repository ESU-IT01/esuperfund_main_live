DELIMITER $$

DROP TRIGGER /*!50032 IF EXISTS */ `tracking_update_broker_notmatched_accounts`$$

CREATE TRIGGER `tracking_update_broker_notmatched_accounts` AFTER UPDATE ON `broker_notmatched_accounts` 
    FOR EACH ROW BEGIN
      IF (new.MatchStatus = 2 AND old.MatchStatus in (1, 3, 4, 5))
      THEN
        INSERT INTO fund_change_tracking(table_name,action_flag,source_id,action_time)
        VALUES('broker_notmatched_accounts','I',new.Id,NOW());
      END IF;
      
      IF (new.MatchStatus = 1 AND old.MatchStatus = 2)
      THEN
        INSERT INTO fund_change_tracking(table_name,action_flag,source_id,action_time)
        VALUES('broker_notmatched_accounts','D',new.Id,NOW());
      END IF;
      
      IF (CHAR_LENGTH(old.settlementAcctNo) <= 0 AND CHAR_LENGTH(new.settlementAcctNo) > 0 AND old.DataType = 'EBROKING')
      THEN
	INSERT INTO fund_change_tracking(table_name,action_flag,source_id,action_time)
        VALUES('broker_notmatched_accounts','U',new.Id,NOW());
      END IF;
END;
$$

DELIMITER ;