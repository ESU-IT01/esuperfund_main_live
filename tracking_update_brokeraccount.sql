DELIMITER $$

DROP TRIGGER /*!50032 IF EXISTS */ `tracking_update_brokeraccount`$$

CREATE TRIGGER `tracking_update_brokeraccount` AFTER UPDATE ON `fundbrokeraccount` 
    FOR EACH ROW BEGIN
      IF (new.fundid <> old.fundid OR IFNULL(new.AccountTypeId,'') <> IFNULL(old.AccountTypeId,'')  OR IFNULL(new.BrokerName,'') <> IFNULL(old.BrokerName,'')
       OR IFNULL(new.AccountNumber,'') <> IFNULL(old.AccountNumber,'')  OR IFNULL(new.Password,'') <> IFNULL(old.Password,'') OR IFNULL(new.CreateTime,'') <> IFNULL(old.CreateTime,'')
       OR IFNULL(new.IsDeleted,'') <> IFNULL(old.IsDeleted,'') OR IFNULL(new.IsExpired,'') <> IFNULL(old.IsExpired,'') OR IFNULL(new.ExpirationDate,'') <> IFNULL(old.ExpirationDate,'')
       OR IFNULL(new.HIN,'') <> IFNULL(old.HIN,'') OR IFNULL(new.CreationDate,'') <> IFNULL(old.CreationDate,'') OR IFNULL(new.SettleBankId,'') <> IFNULL(old.SettleBankId,'') OR IFNULL(new.AccountName,'') <> IFNULL(old.AccountName,'')
    )
     THEN
        INSERT INTO fund_change_tracking(table_name,action_flag,source_id,action_time)
        VALUES('fundbrokeraccount','U',new.Id,NOW());
     END IF;
END;
$$

DELIMITER ;