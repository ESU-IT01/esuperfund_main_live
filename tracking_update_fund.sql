DELIMITER $$

USE `esuperfund_main_live` $$

DROP TRIGGER /*!50032 IF EXISTS */ `tracking_update_fund` $$

CREATE TRIGGER `tracking_update_fund` AFTER UPDATE 
ON `fund` FOR EACH ROW 
BEGIN
  IF (
    IFNULL(new.fundname, '') <> IFNULL(old.fundname, '') 
    OR IFNULL(new.FundTFN, '') <> IFNULL(old.FundTFN, '') 
    OR IFNULL(new.ABN, '') <> IFNULL(old.ABN, '') 
    OR IFNULL(new.EmailAddress, '') <> IFNULL(old.EmailAddress, '') 
    OR IFNULL(new.EstabDate, '') <> IFNULL(old.EstabDate, '') 
    OR IFNULL(new.IsFreeFirstYear, '') <> IFNULL(old.IsFreeFirstYear, '') 
    OR IFNULL(new.IsFreeSecondYear, '') <> IFNULL(old.IsFreeSecondYear, '') 
    OR IFNULL(new.IsBlacklist, '') <> IFNULL(old.IsBlacklist, '') 
    OR IFNULL(new.FundReceivedDate, '') <> IFNULL(old.FundReceivedDate, '') 
    OR IFNULL(new.FundApprovedDate, '') <> IFNULL(old.FundApprovedDate, '') 
    OR IFNULL(new.SMSFName, '') <> IFNULL(old.SMSFName, '') 
    OR IFNULL(new.ExistingOrNewSMSF, '') <> IFNULL(old.ExistingOrNewSMSF, '') 
    OR IFNULL(new.PostalAddress, '') <> IFNULL(old.PostalAddress, '') 
    OR IFNULL(new.PostalSuburb, '') <> IFNULL(old.PostalSuburb, '') 
    OR IFNULL(new.PostalState, '') <> IFNULL(old.PostalState, '') 
    OR IFNULL(new.PostalPostcode, '') <> IFNULL(old.PostalPostcode, '') 
    OR IFNULL(new.ResidentialAddress, '') <> IFNULL(old.ResidentialAddress, '') 
    OR IFNULL(new.ResidentialSuburb, '') <> IFNULL(old.ResidentialSuburb, '') 
    OR IFNULL(new.ResidentialState, '') <> IFNULL(old.ResidentialState, '') 
    OR IFNULL(new.ResidentialPostcode, '') <> IFNULL(old.ResidentialPostcode, '') 
    OR IFNULL(new.HomePhone, '') <> IFNULL(old.HomePhone, '') 
    OR IFNULL(new.Mobile, '') <> IFNULL(old.Mobile, '') 
    OR IFNULL(new.Fax, '') <> IFNULL(old.Fax, '') 
    OR IFNULL(new.TrusteeType, '') <> IFNULL(old.TrusteeType, '') 
    OR IFNULL(new.isDelete, '') <> IFNULL(old.isDelete, '') 
    OR IFNULL(new.IsSetupComplete, '') <> IFNULL(old.IsSetupComplete, '') 
    OR IFNULL(new.SetupCompleteDate, '') <> IFNULL(old.SetupCompleteDate, '') 
    OR IFNULL(new.BlacklistReasonID, '') <> IFNULL(old.BlacklistReasonID, '') 
    OR IFNULL(new.InitialSetupPackage, '') <> IFNULL(old.InitialSetupPackage, '') 
    OR IFNULL(new.SMSFLoginPassword, '') <> IFNULL(old.SMSFLoginPassword, '') 
    OR IFNULL(new.ApplyDate, '') <> IFNULL(old.ApplyDate, '') 
    OR IFNULL(new.BlacklistDate, '') <> IFNULL(old.BlacklistDate, '') 
    OR IFNULL(new.FinalSetupPackage, '') <> IFNULL(old.FinalSetupPackage, '') 
    OR IFNULL(new.LastUpdateTime, '') <> IFNULL(old.LastUpdateTime, '') 
    OR IFNULL(new.FundNoteDetail, '') <> IFNULL(old.FundNoteDetail, '') 
    OR IFNULL(new.InvoiceOnly, '') <> IFNULL(old.InvoiceOnly, '') 
    OR IFNULL(new.SMSFLoginPasswordNew, '') <> IFNULL(old.SMSFLoginPasswordNew, '') 
    OR IFNULL(new.FirstITRYear, '') <> IFNULL(old.FirstITRYear, '') 
    OR IFNULL(new.LastFinancialYear, '') <> IFNULL(old.LastFinancialYear, '') 
    OR IFNULL(new.FundStatusID, '') <> IFNULL(old.FundStatusID, '') 
    OR IFNULL(new.PromotionStatusID, '') <> IFNULL(old.PromotionStatusID, '')    
    OR IFNULL(new.ISPSentDate, '') <> IFNULL(old.ISPSentDate, '') 
    OR IFNULL(new.IsCompanySecretary, '') <> IFNULL(old.IsCompanySecretary, '') 
    OR IFNULL(new.NnfundID, '') <> IFNULL(old.NnfundID, '')
  ) 
  THEN 
  INSERT INTO fund_change_tracking (
    table_name,
    action_flag,
    source_id,
    action_time
  ) 
  VALUES
    ('fund', 'U', new.Id, NOW()) ;
  END IF ;
END ;
$$

DELIMITER ;

