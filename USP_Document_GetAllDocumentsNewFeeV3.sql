DELIMITER $$

USE `esuperfund_main_live`$$

DROP PROCEDURE IF EXISTS `USP_Document_GetAllDocumentsNewFeeV3`$$

CREATE PROCEDURE `USP_Document_GetAllDocumentsNewFeeV3`(v_fundid INT,v_year INT,v_type VARCHAR(50),v_status VARCHAR(50),v_LoanID INT)
BEGIN
	DECLARE v_fundyear INT DEFAULT 0;
	DECLARE v_issetupcomplete INT DEFAULT 0;
	DECLARE v_fundstatusID INT DEFAULT 0; 
	DECLARE v_fycount INT DEFAULT 0;
	DECLARE v_isneedac INT DEFAULT 0;
	DECLARE v_fyid INT DEFAULT 0;
	DECLARE v_fy INT DEFAULT 0;
	DECLARE v_checkliststatus VARCHAR(100) DEFAULT '';
	DECLARE v_lodgeid INT DEFAULT 0;
	DECLARE v_ayear INT DEFAULT 0;
	DECLARE v_minyear INT DEFAULT 0;
	DECLARE v_compliancereviewcount INT DEFAULT 0;
	DECLARE v_count INT DEFAULT 0;
	DECLARE v_clientname VARCHAR(100) DEFAULT '';
	DECLARE v_date1 DATETIME;
	DECLARE v_where VARCHAR(5000) DEFAULT ' ';
	DECLARE v_appid INT DEFAULT 0;
	DECLARE v_setupcompletedate DATETIME;
	DROP TABLE IF EXISTS temp_documents_table;
	CREATE TEMPORARY TABLE temp_documents_table(
		t_date VARCHAR(30),
		t_documenttype VARCHAR(100),
		t_documentdescription VARCHAR(200),
		t_clientname VARCHAR(100),
		t_filename VARCHAR(200),
		t_status VARCHAR(50),
		t_keyid INT,
		t_documenttypeid INT,
		t_year VARCHAR(30),
		t_rnr VARCHAR(10) DEFAULT 'no',
		t_ischild INT DEFAULT 0,
		t_isverified INT DEFAULT 1,
		t_fundstatusid INT
	);
	SELECT FundStatusID,issetupcomplete, EXTRACT(YEAR FROM (CASE LOWER(ExistingOrNewSMSF) WHEN 'new smsf' THEN  (EstabDate+INTERVAL 184 DAY)  ELSE  (Applydate+INTERVAL 184 DAY)  END)),(CASE LOWER(ExistingOrNewSMSF) WHEN 'new smsf' THEN  EstabDate  ELSE ApplyDate  END),SetupCompleteDate
	INTO v_fundstatusID,v_issetupcomplete,v_fundyear,v_date1,v_setupcompletedate FROM fund WHERE id=v_fundid;
	SELECT COUNT(*) INTO v_fycount FROM cl_fy_fundfinancialyear WHERE fundid=v_fundid;
	SELECT MAX(`year`),MIN(`year`) INTO v_ayear,v_minyear FROM cl_fy_fundfinancialyear WHERE fundid=v_fundid;
	SET v_count= v_fycount;
	
	/*SMSF Setup*/
		IF(v_type='SMSF Setup' OR v_type='All') THEN
			CALL USP_Document_GetAllDocumentsNew_SMSFSetup(v_fundid,v_fundstatusID,v_issetupcomplete,v_fundyear,v_date1);
		END IF;
	/*Annual Compliance Documents*/
		IF(v_type='Annual Compliance Documents' OR v_type='All') THEN
			WHILE v_ayear>v_minyear-1 DO 
				CALL USP_Document_GetAllDocumentsNew_AnnualComplianceDocuments(v_fundid,v_ayear);
				SET v_fycount=v_fycount-1;
				SET v_ayear=v_ayear-1;
			END WHILE;
			UPDATE temp_documents_table SET t_documenttypeid=1002 WHERE t_documenttype='Annual Compliance Documents';
			SET v_fycount=v_count;
		END IF;
	/*Applications*/
		IF(v_type='Applications' OR v_type='All') THEN
			/**Change Bank**/
			INSERT INTO temp_documents_table(t_date,t_documenttype,t_documentdescription,t_clientname,t_filename,t_status,t_documenttypeid,t_year)
			SELECT CASE IFNULL(c.LastUpdatetime,'') WHEN ''
			 THEN b.SubmitDate ELSE c.LastUpdatetime END,'Applications',
				CASE c.trustdeedtypeid  WHEN 302 THEN 'CBA ACA Application' ELSE 'ANZ V2 Plus Application' END,'N/A',
				CONCAT(CASE c.IsSignedUploaded WHEN 1 THEN c.SignedFileName  ELSE c.UnsignedFileName END,'!!~/App_Data/ChangeBankFile/'),
				CASE b.StatusId WHEN 4 THEN 'Complete' ELSE 'Incomplete' END,1003,EXTRACT(YEAR FROM (b.submitdate+INTERVAL 184 DAY))
			FROM fund_mergeupdate c,changebank_history b WHERE c.id=b.fund_mergeupdate_id AND b.StatusID!=5 AND IFNULL(b.IsExpired,0)=0 AND b.fundid=v_fundid AND c.fundid=v_fundid AND c.trustdeedtypeid IN (301,302) ORDER BY b.SubmitDate DESC;
			/**Term Deposit**/
			INSERT INTO temp_documents_table(t_date,t_documenttype,t_documentdescription,t_clientname,t_filename,t_status,t_documenttypeid,t_year)
			SELECT c.LastUpdatetime,'Applications','Term Deposit Application','N/A',
				CONCAT(CASE c.IsSignedUploaded WHEN 1 THEN c.SignedFileName  ELSE c.UnsignedFileName END,'!!~/App_Data/TD/barcodedocs/termdepositfiles/'),
				CASE a.StatusID WHEN 8 THEN 'Complete' ELSE 'Incomplete' END,1003,EXTRACT(YEAR FROM (b.submitdate+INTERVAL 184 DAY))
			FROM bank_management a LEFT JOIN application_history b ON a.applicationid=b.id LEFT JOIN fund_mergeupdate c ON a.FundMergeUpdateID=c.id LEFT JOIN fundbankaccount d ON a.fundbankaccountid=d.id
			WHERE a.applicationid=b.id AND b.Application_TypeID=4 AND a.StatusID !=10 AND IFNULL(a.isdeleted,0)=0 AND IFNULL(b.isdeleted,0)=0 AND a.fundbankaccountid=d.id AND IFNULL(d.isdeleted,0)=0 AND b.fundid=c.fundid AND b.fundid=v_fundid ORDER BY c.LastUpdatetime DESC;
			/**Centrelink**/
			INSERT INTO temp_documents_table(t_date,t_documenttype,t_documentdescription,t_clientname,t_filename,t_status,t_documenttypeid,t_year)
			SELECT c.LastUpdatetime,'Applications','Centrelink Application',CONCAT(b.FirstName,' ',b.LastName),
				CASE IFNULL(c.UnsignedFileName,'') WHEN '' THEN '' ELSE  CONCAT(c.UnsignedFileName,'!!~/App_Data/CentrelinkDocuments/') END,
				CASE IFNULL(c.UnsignedFileName,'') WHEN '' THEN 'Incomplete' ELSE 'Complete' END,1003,EXTRACT(YEAR FROM (a.ApplicationApplyDate+INTERVAL 184 DAY))
			FROM centrelink_processed_app a,fundtrustee b ,fund_mergeupdate AS c
			WHERE c.FundID=b.FundID AND c.FundID=a.fundID AND a.memberID=b.ID AND a.mergedocid=c.id AND a.fundID=v_fundid ORDER BY c.LastUpdatetime DESC;
			/**ing**/
			INSERT INTO temp_documents_table(t_date,t_documenttype,t_documentdescription,t_clientname,t_filename,t_status,t_documenttypeid,t_year)
			SELECT c.LastUpdatetime,'Applications','ING Online Saver Application','N/A',
				CONCAT(CASE IFNULL(c.SignedFileName,'') WHEN '' THEN c.UnsignedFileName  ELSE c.SignedFileName END,'!!~/App_Data/ING/'),
				CASE a.StatusID WHEN 8 THEN 'Complete' ELSE 'Incomplete' END,1003,EXTRACT(YEAR FROM (b.submitdate+INTERVAL 184 DAY))
			FROM bank_management a LEFT JOIN application_history b ON a.applicationid=b.id LEFT JOIN fund_mergeupdate c ON a.FundMergeUpdateID=c.id
			WHERE a.applicationid=b.id AND b.Application_TypeID=6 AND IFNULL(a.isdeleted,0)=0 AND IFNULL(b.isdeleted,0)=0 AND a.StatusId!=10 AND b.fundid=c.fundid AND b.fundid=v_fundid ORDER BY c.LastUpdatetime DESC;
			/**Change Broker**/
			INSERT INTO temp_documents_table(t_date,t_documenttype,t_documentdescription,t_clientname,t_filename,t_status,t_documenttypeid,t_year)
			SELECT c.LastUpdatetime,'Applications','Change Broker Application','N/A',
				CONCAT(CASE c.IsSignedUploaded WHEN 1 THEN c.SignedFileName  ELSE c.UnsignedFileName END,'!!~/App_Data/ChangeBrokerFile/'),
				CASE a.StatusID WHEN 8 THEN 'Complete' ELSE 'Incomplete' END,1003,EXTRACT(YEAR FROM (b.submitdate+INTERVAL 184 DAY))  
			FROM broker_management a LEFT JOIN application_history b ON a.applicationid=b.id LEFT JOIN fund_mergeupdate c ON a.FundMergeUpdateID=c.id 
			WHERE a.applicationid=b.id AND b.Application_TypeID=2 AND a.StatusID !=10 AND IFNULL(a.isdeleted,0)=0 AND IFNULL(b.isdeleted,0)=0 AND b.fundid=c.fundid AND b.fundid=v_fundid AND a.statusid IS NOT NULL ORDER BY c.LastUpdatetime DESC;
			
			/**CommSec Options & CommSec Warrants**/
			INSERT INTO temp_documents_table(t_date,t_documenttype,t_documentdescription,t_clientname,t_filename,t_status,t_documenttypeid,t_year)
			SELECT d.LastUpdatetime,'Applications',CASE c.Application_TypeID WHEN 7 THEN 'CommSec Options' ELSE 'CommSec Warrants' END, 'N/A', 
				CONCAT(d.UnsignedFileName, CASE c.Application_TypeID WHEN 7 THEN '!!~/App_Data/CommSecOptions/file/' ELSE '!!~/App_Data/CommSecWarrants/file/' END),
				'Complete', 1003, EXTRACT(YEAR FROM (c.submitdate + INTERVAL 184 DAY))
			FROM broker_management a JOIN fundbrokeraccount b ON a.FundBrokerAccountID = b.id JOIN application_history c ON a.applicationid = c.id JOIN fund_mergeupdate d ON a.FundMergeUpdateID = d.id
			WHERE c.Application_TypeID IN (7, 8) AND a.StatusID != 10 AND IFNULL(a.isdeleted, 0) = 0 AND IFNULL(c.isdeleted, 0) = 0 AND c.fundid = d.fundid AND c.fundid = v_fundid AND a.statusid IS NOT NULL ORDER BY d.LastUpdatetime DESC;
            
			/**Trustee/Member Changes**/
			SELECT ID INTO v_appid FROM application_history WHERE FundID = v_fundid AND Application_TypeID = 3 AND IFNULL(IsDeleted,0) = 0 ORDER BY SubmitDate DESC LIMIT 0,1;
			IF v_appid>0 THEN
				CALL USP_Document_GetAllDocumentsNew_TrusteeMemberChanges(v_appid,v_status);
			END IF;
		END IF;
	/*Fees*/
		/*IF(v_type='Fees' OR v_type='All' or v_type='SMSF Setup') THEN
			INSERT INTO temp_documents_table(t_date,t_documenttype,t_documentdescription,t_clientname,t_filename,t_status,t_documenttypeid,t_year,t_keyid)
			SELECT (CASE a.FeeTypeID WHEN 9 THEN (SELECT EstabDate FROM fund f WHERE f.id=v_fundid) ELSE duedate END) AS duedate,'Fees', CONCAT(CASE a.FeeTypeID WHEN 1 THEN 'Annual Compliance Fees' WHEN 2 THEN 'Trust Deed Update' WHEN 3 THEN 'Actuarial Fee' 
				WHEN 4 THEN 'Exist Fee' WHEN 5 THEN 'Loan Application Fee' WHEN 6 THEN 'Other Fees' WHEN 7 THEN 'Company Setup Fee' WHEN 8 THEN 'Bare Trust / Security Custodian Deed Fee'  WHEN 9 THEN 'SMSF Setup'  END,IF(a.FeeTypeID<>9,' - ',""),IF(a.FeeTypeID<>9,a.year,"")),'N/A','',CASE a.FeeTypeID WHEN 9 THEN IF(v_fundstatusID = 4,'Complete','Incomplete') ELSE 'Complete'  END,1004,a.year,a.id
			FROM admin_fund_compliancefees a WHERE a.fundid=v_fundid AND a.year<=YEAR(NOW()+INTERVAL 184 DAY)  AND (a.FeeTypeId!=8 OR a.Is_WaivedFeePaid!=1 ) ORDER BY a.duedate DESC;  
		END IF;*/
	/*Rollovers*/
		IF(v_type='Rollovers' OR v_type='All') THEN
			INSERT INTO temp_documents_table(t_date,t_documenttype,t_documentdescription,t_clientname,t_filename,t_status,t_documenttypeid,t_year)
			SELECT  a.CreateTime ,'Rollovers', b.typename, func_GetTrusteeName(c.fundmemberid),CONCAT(a.FileName,'!!~/App_Data/SMSFDocuments/'),'Complete',1006,EXTRACT(YEAR FROM (a.CreateTime+INTERVAL 184 DAY))      
			FROM fund_upload_files a , fund_document_base b,fund_rollover c 
			WHERE a.typeid =b.foreigntypeid AND a.ForeignID=c.id AND b.foreigntablename='fund_upload_files' AND  a.fundid=v_fundid AND b.foreigntypeid=99;
		END IF;
	/*Death Benefit Agreements*/
		IF(v_type='Death Benefit Agreements' OR v_type='All') THEN
			INSERT INTO temp_documents_table(t_date,t_documenttype,t_documentdescription,t_clientname,t_filename,t_status,t_documenttypeid,t_year)
			SELECT  a.ApplyDate,'Death Benefit Agreements','Death Benefit Agreements',CONCAT(b.FirstName,' ',b.LastName),
				CONCAT(CASE c.IsSignedUploaded WHEN 1 THEN c.SignedFileName ELSE c.UnsignedFileName END,'!!~/App_Data/BDADocs/'),
				CASE c.IsSignedUploaded WHEN 1 THEN CASE c.IsUnsignedMerged WHEN 1 THEN 'Complete' ELSE  'Incomplete' END ELSE 'Incomplete' END,1007,EXTRACT(YEAR FROM (a.ApplyDate+INTERVAL 184 DAY))
			FROM bda_application AS a,fundtrustee AS b,fund_mergeupdate AS c
			WHERE c.FundID=b.FundID AND c.FundID=a.fundID AND a.memberID=b.ID AND a.bda_mergeupdate_id=c.ID AND (a.StatusID=3 OR a.StatusID=4) AND a.fundID=v_fundid;
		END IF;
	/*ATO Documents*/
		IF(v_type='ATO Documents' OR v_type='All') THEN
			IF v_fundstatusID>=3 THEN
				IF v_setupcompletedate>'2017-03-10'|| IFNULL(v_setupcompletedate,'')='' THEN
					INSERT INTO temp_documents_table(t_date,t_documenttype,t_documentdescription,t_clientname,t_filename,t_status,t_documenttypeid,t_year,t_isverified)
					SELECT CASE IFNULL(b.CreateTime,'0') WHEN '0' THEN CONCAT(v_fundyear,'-07-01') ELSE DATE_FORMAT(b.CreateTime,'%Y-%m-%d') END,'ATO Documents',a.typename,'N/A',CONCAT(b.filename,'!!~/App_Data/SMSFDocuments/'),
						CASE v_issetupcomplete WHEN 1 THEN 'Complete' ELSE 'Incomplete' END,1009,
						CASE IFNULL(b.CreateTime,'0') WHEN '0' THEN v_fundyear ELSE EXTRACT(YEAR FROM (b.CreateTime+INTERVAL 184 DAY)) END,IFNULL(b.IsVerified,1)
					FROM (SELECT * FROM fund_document_base 
					WHERE id =113 ORDER BY sortorder) a LEFT JOIN (SELECT * FROM (SELECT * FROM fund_upload_files WHERE fundid=v_fundid AND IFNULL(isdelete,0)=0 ORDER BY CreateTime DESC) c GROUP BY typeid) b ON a.foreigntypeid=b.typeid;
				ELSE 
					INSERT INTO temp_documents_table(t_date,t_documenttype,t_documentdescription,t_clientname,t_filename,t_status,t_documenttypeid,t_year,t_isverified)
					SELECT CASE IFNULL(b.CreateTime,'0') WHEN '0' THEN CONCAT(v_fundyear,'-07-01') ELSE DATE_FORMAT(b.CreateTime,'%Y-%m-%d') END,'ATO Documents',a.typename,'N/A',CONCAT(b.filename,'!!~/App_Data/SMSFDocuments/'),
						CASE v_issetupcomplete WHEN 1 THEN 'Complete' ELSE 'Incomplete' END,1009,
						CASE IFNULL(b.CreateTime,'0') WHEN '0' THEN v_fundyear ELSE EXTRACT(YEAR FROM (b.CreateTime+INTERVAL 184 DAY)) END,IFNULL(b.IsVerified,1)
					FROM (SELECT * FROM fund_document_base 
					WHERE id IN(108,109,110,113) ORDER BY sortorder) a LEFT JOIN (SELECT * FROM (SELECT * FROM fund_upload_files WHERE fundid=v_fundid AND IFNULL(isdelete,0)=0 ORDER BY CreateTime DESC) c GROUP BY typeid) b ON a.foreigntypeid=b.typeid;
				END IF;	
			END IF;
			/**Contributions**/
			INSERT INTO temp_documents_table(t_date,t_documenttype,t_documentdescription,t_clientname,t_filename,t_status,t_documenttypeid,t_year)
			SELECT a.CreateTime, 'ATO Documents', CASE c.description WHEN 'Co-Contribution Remittance Advice' THEN 'Co-Contributions Remittance Advice' ELSE c.description END,
				func_GetTrusteeNameByContributionId(b.id, 'contribution_co_member'),CONCAT(a.FileName,'!!~/App_Data/SMSFDocuments/'),'Complete',1009,EXTRACT(YEAR FROM (a.CreateTime+INTERVAL 184 DAY))     
			FROM fund_upload_files a INNER JOIN contribution_co b ON a.foreignid=b.id INNER JOIN fund_document_base c ON a.typeid=c.foreigntypeid
			WHERE a.fundid=v_fundid AND  a.isDelete=0 AND a.FileName<>'undefined' AND a.typeid=100;
			INSERT INTO temp_documents_table(t_date,t_documenttype,t_documentdescription,t_clientname,t_filename,t_status,t_documenttypeid,t_year)
			SELECT a.CreateTime, 'ATO Documents', CASE c.description WHEN 'Co-Contribution Remittance Advice' THEN 'Co-Contributions Remittance Advice' ELSE c.description END,
				func_GetTrusteeNameByContributionId(b.id, 'contribution_lisc_member'),CONCAT(a.FileName,'!!~/App_Data/SMSFDocuments/'),'Complete',1009,EXTRACT(YEAR FROM (a.CreateTime+INTERVAL 184 DAY))      
			FROM fund_upload_files a INNER JOIN contribution_lisc b ON a.foreignid=b.id INNER JOIN fund_document_base c ON a.typeid=c.foreigntypeid
			WHERE a.fundid=v_fundid AND  a.isDelete=0 AND a.FileName<>'undefined' AND a.typeid=101;
		END IF;
	/*Trust Deed*/
		IF(v_type='Trust Deed' OR v_type='All') THEN
			INSERT INTO temp_documents_table(t_date,t_documenttype,t_documentdescription,t_clientname,t_filename,t_status,t_keyid,t_documenttypeid,t_year,t_ischild)
			SELECT b.LastUpdatetime,'SMSF Setup',CASE b.EffectiveRangeID  
				WHEN 1 THEN 'Trust Deed' 
				WHEN 2 THEN  (CASE b.IsDeedUpgrade WHEN 1 THEN'Amended Trust Deed' ELSE 'Trust Deed' END) END,'N/A',
				CASE b.IsDeedUpgrade WHEN 1  THEN 
				(CASE LENGTH(b.SignedFileName) WHEN 0 THEN CONCAT(b.UnSignedFileName,'!!~/App_Data/TrustDeedFile/') ELSE CONCAT(b.SignedFileName,'!!~/App_Data/TrustDeedFile/') END) ELSE
				(CASE LENGTH(b.SignedFileName) WHEN 0 THEN '' ELSE CONCAT(b.SignedFileName,'!!~/App_Data/TrustDeedFile/') END) END,
				CASE b.IsDeedUpgrade WHEN 1 THEN
				(CASE b.issigneduploaded WHEN 1 THEN 'Complete' ELSE 'Incomplete' END) ELSE
				(CASE v_issetupcomplete WHEN 1 THEN 'Complete' ELSE 'Incomplete' END) END,b.id,
				CASE b.EffectiveRangeID  WHEN 1 THEN -1 WHEN 2 THEN -2 END,EXTRACT(YEAR FROM (b.LastUpdatetime+INTERVAL 184 DAY)),1
			FROM fund_mergeupdate b WHERE fundid=v_fundid AND b.EffectiveRangeID IN(1,2) AND b.trustdeedtypeid<300;
		END IF;
	/*Investment Strategy*/
		IF(v_type='Investment Strategy' OR v_type='All') THEN
			/*IF v_fundstatusID>=3 THEN*/
			INSERT INTO temp_documents_table(t_date,t_documenttype,t_documentdescription,t_clientname,t_filename,t_status,t_keyid,t_documenttypeid,t_year,t_ischild)
			SELECT b.LastUpdatetime,'SMSF Setup',CASE b.EffectiveRangeID  
				WHEN 3 THEN  'Investment Strategy' 
				WHEN 4 THEN  (CASE b.IsDeedUpgrade WHEN 1 THEN 'Amended Investment Strategy' ELSE 'Investment Strategy' END) END,'N/A',
				CASE b.IsDeedUpgrade WHEN 1  THEN 
				(CASE LENGTH(b.SignedFileName) WHEN 0 THEN CONCAT(b.UnSignedFileName,'!!~/App_Data/TrustDeedFile/') ELSE CONCAT(b.SignedFileName,'!!~/App_Data/TrustDeedFile/') END) ELSE
				(CASE LENGTH(b.SignedFileName) WHEN 0 THEN '' ELSE CONCAT(b.SignedFileName,'!!~/App_Data/TrustDeedFile/') END) END,
				CASE b.IsDeedUpgrade WHEN 1 THEN 
				(CASE b.issigneduploaded WHEN 1 THEN 'Complete' ELSE 'Incomplete' END) ELSE
				(CASE v_issetupcomplete WHEN 1 THEN 'Complete' ELSE 'Incomplete' END) END,b.id,
				CASE b.EffectiveRangeID  WHEN 3 THEN -3 WHEN 4 THEN -4 END,EXTRACT(YEAR FROM (b.LastUpdatetime+INTERVAL 184 DAY)),1
			FROM fund_mergeupdate b WHERE fundid=v_fundid AND b.EffectiveRangeID IN(3,4) AND b.trustdeedtypeid<300;
			/*END IF;*/
		END IF;
	/*Loans*/
		IF(v_type='Loans' OR v_type='All') THEN
			CALL USP_Document_GetAllDocumentsNew_Loan(v_fundid,v_LoanID);
		END IF;
	/*Company Setup*/
		IF(v_type='Company Setup' OR v_type='All') THEN
			INSERT INTO temp_documents_table(t_date,t_documenttype,t_documentdescription,t_clientname,t_filename,t_status,t_documenttypeid,t_year)
			SELECT a.LastUpdatetime,'Company Setup', b.TypeName AS v_documentdescription,'N/A',
				CONCAT(a.UnsignedFileName,'!!~/App_Data/Ecompanies/'),'Complete',1015,EXTRACT(YEAR FROM (a.LastUpdatetime+INTERVAL 184 DAY))
			FROM fund_mergeupdate a INNER JOIN fund_document_base b ON a.TrustDeedTypeID = b.ID INNER JOIN fund_document_base c ON b.ParentTypeID = c.ID 
			WHERE a.TrustDeedTypeID = b.ID AND b.ParentTypeID = 1015 AND a.FundID = v_fundid;
		END IF;
	/*Account Linkage Request*/
		IF(v_type='Account Linkage Request' OR v_type='All') THEN
			INSERT INTO temp_documents_table(t_date,t_documenttype,t_documentdescription,t_clientname,t_filename,t_status,t_documenttypeid,t_year)
			SELECT a.LastUpdatetime,'Account Linkage Request', b.Description AS v_documentdescription,'N/A',
				(a.UnSignedFileName),
				(CASE a.issigneduploaded WHEN 1 THEN 'Complete' ELSE 'Incomplete' END),
				1016,EXTRACT(YEAR FROM (a.LastUpdatetime+INTERVAL 184 DAY))
			FROM fund_mergeupdate a INNER JOIN fund_merge_base_type b ON a.TrustDeedTypeID = b.ID JOIN fund_document_base c ON b.DocumentBaseID = c.`ID`
			WHERE a.FundID = v_fundid AND c.`ParentTypeID` = 1016;
		END IF;
	/*Others*/
		IF(v_type='Others' OR v_type='All') THEN
			INSERT INTO temp_documents_table(t_date,t_documenttype,t_documentdescription,t_clientname,t_filename,t_status,t_keyid,t_documenttypeid,t_year)
			SELECT CreateTime,'Others', SPLIT_STR(FileDisplayName,'|',2),'N/A',
				CONCAT(filename,'!!~/App_Data/Upload/OtherDocuments/'),'Complete',id,10000,EXTRACT(YEAR FROM (CreateTime+INTERVAL 184 DAY))
			FROM fund_upload_files 
			WHERE FundID = v_fundid AND typeid=0 AND isdelete=0;
		END IF;
	IF(v_year>0) THEN
		SET v_where=CONCAT(v_where,'t_year=',v_year,' and ');
	END IF;
	/*if(v_status='Complete') then
		set v_where=concat(v_where,"((t_documenttype!='Annual Compliance Documents' and t_status='Complete') 
					    or (t_documenttype='Annual Compliance Documents' and t_status!='Ready for Signing' or (t_status='Ready for Signing' and LOCATE('trustee declaration',t_documentdescription)=0 and LOCATE('electronic lodgement declaration',t_documentdescription)=0))) and ");
	elseif(v_status='Incomplete') then
		set v_where=concat(v_where,"((t_documenttype!='Annual Compliance Documents' and t_status!='Complete') 
					    or (t_documenttype='Annual Compliance Documents' and t_status='Ready for Signing' and (LOCATE('trustee declaration',t_documentdescription)>0 or LOCATE('electronic lodgement declaration',t_documentdescription)>0))) and ");
	end if;*/
        IF(v_status='Complete') THEN
		SET v_where=CONCAT(v_where,"((t_documenttype!='Annual Compliance Documents' and t_status='Complete') or 
					     (t_documenttype='Annual Compliance Documents' and t_status!='Ready for Signing' or 
					     (t_status='Ready for Signing' and  ((LOCATE('trustee declaration',t_documentdescription)=0 and LOCATE('electronic lodgement declaration',t_documentdescription)=0)) or (LOCATE('(Signed)',t_documentdescription)>0 )))
					     ) and ");
	ELSEIF(v_status='Incomplete') THEN
		SET v_where=CONCAT(v_where,"((t_documenttype!='Annual Compliance Documents' and t_status!='Complete') or 
					     (t_documenttype='Annual Compliance Documents' and t_status='Ready for Signing' and (((LOCATE('trustee declaration',t_documentdescription)>0 or LOCATE('electronic lodgement declaration',t_documentdescription)>0)) and (LOCATE('(Signed)',t_documentdescription)=0 )))
					     ) and ");
	END IF;
	IF(v_type!='All') THEN
		IF(v_type='Trust Deed') THEN
			SET v_where=CONCAT(v_where,'(t_documenttypeid=-1 or t_documenttypeid=-2) and t_ischild=1',' and ');
		ELSEIF(v_type='Investment Strategy') THEN
			SET v_where=CONCAT(v_where,'(t_documenttypeid=-3 or t_documenttypeid=-4) and t_ischild=1',' and ');
		ELSEIF(v_type='Loans') THEN
			SET v_where=CONCAT(v_where,"t_documenttype='loans' and t_ischild=0 and t_keyid=",v_LoanID," and ");
		ELSEIF(v_type='SMSF Setup') THEN
			SET v_where=CONCAT(v_where,"(t_documenttype='",v_type,"' or t_documenttype='Company Setup'  or (t_documenttype='Fees' AND t_documentdescription = 'SMSF Setup' ))  and t_ischild=0",' and ');
		ELSE
			SET v_where=CONCAT(v_where,"t_documenttype='",v_type,"' and t_ischild=0",' and ');
		END IF;
	ELSE
		SET v_where=CONCAT(v_where,"t_ischild=0 and ");
	END IF;
	SET v_where=CONCAT(v_where,'1=1');
	SET @strSQL=CONCAT('select * from temp_documents_table where ',v_where);  
 	
	PREPARE stmt1 FROM @strSQL;  
	EXECUTE stmt1;  
	DEALLOCATE PREPARE stmt1; 	
	
	/*select count(*) from temp_documents_table 
	where ((t_documenttype!='Annual Compliance Documents' and t_status='Complete') 
	or (t_documenttype='Annual Compliance Documents' and t_status!='Ready for Signing' or (t_status='Ready for Signing' and LOCATE('trustee declaration',t_documentdescription)=0 and LOCATE('electronic lodgement declaration',t_documentdescription)=0)))  and t_ischild=0;
	select count(*) from temp_documents_table 
	where ((t_documenttype!='Annual Compliance Documents' and t_status!='Complete') 
	or (t_documenttype='Annual Compliance Documents' and t_status='Ready for Signing' and (LOCATE('trustee declaration',t_documentdescription)>0 or LOCATE('electronic lodgement declaration',t_documentdescription)>0))) and t_ischild=0;
*/
	SELECT COUNT(*) FROM temp_documents_table 
	WHERE ((t_documenttype!='Annual Compliance Documents' AND t_status='Complete') OR 
	       (t_documenttype='Annual Compliance Documents' AND t_status!='Ready for Signing' OR 
	       (t_status='Ready for Signing' AND  ((LOCATE('trustee declaration',t_documentdescription)=0 AND LOCATE('electronic lodgement declaration',t_documentdescription)=0)) OR (LOCATE('(Signed)',t_documentdescription)>0 )))
	       ) AND t_ischild=0;
	SELECT COUNT(*) FROM temp_documents_table 
	WHERE ((t_documenttype!='Annual Compliance Documents' AND t_status!='Complete') OR 
	       (t_documenttype='Annual Compliance Documents' AND t_status='Ready for Signing' AND (((LOCATE('trustee declaration',t_documentdescription)>0 OR LOCATE('electronic lodgement declaration',t_documentdescription)>0)) AND (LOCATE('(Signed)',t_documentdescription)=0 )))
	       ) AND t_ischild=0;          
		
	END$$

DELIMITER ;