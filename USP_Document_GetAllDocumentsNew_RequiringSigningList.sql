DELIMITER $$

USE `esuperfund_main_live`$$

DROP PROCEDURE IF EXISTS `USP_Document_GetAllDocumentsNew_RequiringSigningList`$$

CREATE PROCEDURE `USP_Document_GetAllDocumentsNew_RequiringSigningList`(v_fundid INT)
BEGIN
	DECLARE v_ayear INT DEFAULT 0;
	DECLARE v_maxyear INT DEFAULT 0;
	DECLARE v_minyear INT DEFAULT 0;
	DECLARE v_fyid INT DEFAULT 0;
	DECLARE v_lodgeid INT DEFAULT 0;
	DECLARE v_checkliststatus VARCHAR(100) DEFAULT '';
	DECLARE v_fundyear INT DEFAULT 0;
	DECLARE v_fundstatusID INT DEFAULT 0; 
	DECLARE v_ISPSignedCount INT DEFAULT 0;
	DECLARE v_IdCount INT DEFAULT 0; 
	DECLARE v_appDate DATE;
	DROP TABLE IF EXISTS temp_documents_table;
	CREATE TEMPORARY TABLE temp_documents_table(
		t_date VARCHAR(30),
		t_documenttype VARCHAR(100),
		t_documentdescription VARCHAR(200),
		t_clientname VARCHAR(100),
		t_filename VARCHAR(200),
		t_documenttypeid INT,
		t_keyid INT,
		t_typeid INT,
		t_appid INT DEFAULT 0,
		t_memberid INT DEFAULT 0,
		t_bankname VARCHAR(100)
	);
	SELECT FundStatusID, EXTRACT(YEAR FROM (CASE LOWER(ExistingOrNewSMSF) WHEN 'new smsf' THEN  (EstabDate+INTERVAL 184 DAY)  ELSE  (Applydate+INTERVAL 184 DAY)  END))
	INTO v_fundstatusID,v_fundyear FROM fund WHERE id=v_fundid;
	SELECT MAX(`year`),MIN(`year`) INTO v_ayear,v_minyear FROM cl_fy_fundfinancialyear WHERE fundid=v_fundid;
	
	SET v_maxyear = v_ayear;
	/*ISP*/
	IF v_fundstatusID<3 THEN 
		SELECT COUNT(*) INTO v_ISPSignedCount FROM fund_mergeupdate WHERE fundid=v_fundid AND TrustDeedTypeID=2;
		IF (v_ISPSignedCount=0) THEN
			INSERT INTO temp_documents_table(t_date,t_documenttype,t_documentdescription,t_clientname,t_filename,t_keyid,t_typeid)
			SELECT IFNULL(IF(applydate < EstabDate AND EstabDate = '2018-07-01','2018-07-01',UploadedDate),CONCAT(v_fundyear,'-07-01')),'SMSF Setup','SMSF Establishment Documentation - Full Package','N/A',CONCAT(InitialSetupPackage,'!!~/App_Data/ispfile/'),v_fundid,2
			FROM fund WHERE id=v_fundid AND applydate>='2016-11-21';
		END IF;
		SELECT COUNT(*) INTO v_ISPSignedCount FROM fund_mergeupdate WHERE fundid=v_fundid AND TrustDeedTypeID=2 AND IFNULL(SignedFileName,'')='';
		IF (v_ISPSignedCount=1) THEN
			INSERT INTO temp_documents_table(t_date,t_documenttype,t_documentdescription,t_clientname,t_filename,t_keyid,t_typeid)
			SELECT IFNULL(IF(applydate < EstabDate AND EstabDate = '2018-07-01','2018-07-01',applydate),CONCAT(v_fundyear,'-07-01')),'SMSF Setup', 'SMSF Establishment Documentation - Signing Pages Only', 'N/A', CONCAT(b.UnSignedFileName,'!!~/App_Data/ispfile/'),v_fundid,2
			FROM fund_mergeupdate b LEFT JOIN fund a ON b.fundid=a.id WHERE b.fundid=a.id AND fundid=v_fundid AND b.TrustDeedTypeID =2 AND a.applydate>='2016-11-21';
		END IF;
	END IF;
	IF v_fundstatusID<=3 || v_fundstatusID=13 THEN 
		SELECT COUNT(*) INTO v_IdCount FROM fund_upload_files WHERE fundid=v_fundid AND TypeId=7;
		SELECT applydate INTO v_appDate FROM fund WHERE id=v_fundid;
		IF (v_IdCount<1 && v_appDate>'2016-11-21') THEN
		INSERT INTO temp_documents_table(t_date,t_documenttype,t_documentdescription,t_clientname,t_filename,t_keyid,t_typeid)
			SELECT IFNULL(IF(applydate < EstabDate AND EstabDate = '2018-07-01','2018-07-01',applydate),CONCAT(v_fundyear,'-07-01')),'SMSF Setup','Certified Identification','N/A',CONCAT(InitialSetupPackage,'!!~/App_Data/barcodedocs/identificationfiles/'),v_fundid,2
			FROM fund WHERE id=v_fundid;
		END IF;
	END IF;
	/*SMSF Setup*/
	INSERT INTO temp_documents_table(t_date,t_documenttype,t_documentdescription,t_clientname,t_filename,t_keyid,t_typeid)
	SELECT IF(f.applydate < f.EstabDate AND f.EstabDate = '2018-07-01','2018-07-01',b.LastUpdatetime),'SMSF Setup',CASE b.EffectiveRangeID  
		WHEN 2 THEN  'Amended Trust Deed'
		WHEN 4 THEN  'Amended Investment Strategy'  END,'N/A',
		CASE LENGTH(b.UnSignedFileName) WHEN 0 THEN '' ELSE CONCAT(b.UnSignedFileName,'!!~/App_Data/TrustDeedFile/') END,b.id,1001
	FROM fund_mergeupdate b,fund f WHERE b.fundid=f.id AND  b.fundid=v_fundid AND b.EffectiveRangeID IN(2,4) AND b.IsDeedUpgrade=1 AND b.trustdeedtypeid<300 AND LENGTH(b.SignedFileName)=0 ORDER BY b.LastUpdatetime DESC;
	
	/*Annual Compliance Documents*/
	WHILE v_ayear>v_minyear-1 DO 
		SELECT id,LodgementStatusID,ChecklistStatus INTO v_fyid,v_lodgeid,v_checkliststatus FROM cl_fy_fundfinancialyear WHERE fundid=v_fundid AND `year`= v_ayear;
		IF v_fyid>0 AND v_lodgeid=2 AND LOWER(v_checkliststatus)='submitted' THEN
			IF v_ayear <= 2017 THEN
				INSERT INTO temp_documents_table(t_date,t_documenttype,t_documentdescription,t_clientname,t_filename,t_keyid,t_typeid)
				SELECT CreateTime,'Annual Compliance Documents',CONCAT(v_ayear,' Trustee Declaration'),'N/A',CONCAT(FileName,'!!~/nnfunds/WIPReviewSheets/'),v_fyid,5
				FROM cl_fy_wipreview_files WHERE fyid=v_fyid AND typeid=5 AND fyid NOT IN (SELECT fyid FROM cl_fy_wipreview_files WHERE fyid=v_fyid AND typeid=7);
			
			END IF;
			INSERT INTO temp_documents_table(t_date,t_documenttype,t_documentdescription,t_clientname,t_filename,t_keyid,t_typeid)
			SELECT CreateTime,'Annual Compliance Documents',CONCAT(v_ayear,' Electronic Lodgement Declaration'),'N/A',CONCAT(FileName,'!!~/nnfunds/WIPReviewSheets/'),v_fyid,6
			FROM cl_fy_wipreview_files WHERE fyid=v_fyid AND typeid=6 AND fyid NOT IN (SELECT fyid FROM cl_fy_wipreview_files WHERE fyid=v_fyid AND typeid=8);
		END IF;
		SET v_ayear=v_ayear-1;
	END WHILE;
	/*Change Bank*/
	INSERT INTO temp_documents_table(t_date,t_documenttype,t_documentdescription,t_clientname,t_filename,t_keyid,t_typeid,t_appid)
	SELECT CASE IFNULL(b.LastUpdatetime,'') WHEN '' THEN a.SubmitDate ELSE b.LastUpdatetime END,'Change Bank',CASE b.trustdeedtypeid  WHEN 302 THEN 'CBA ACA Application' ELSE 'ANZ V2 Plus Application' END,'N/A',
		CONCAT(b.UnsignedFileName,'!!~/App_Data/ChangeBankFile/'),b.id,1003,a.id
	FROM changebank_history a LEFT JOIN fund_mergeupdate b ON b.id=a.fund_mergeupdate_id
	WHERE IFNULL(a.IsExpired,0)=0 AND b.id=a.fund_mergeupdate_id AND a.statusid=2 AND a.fundid=v_fundid ORDER BY a.SubmitDate DESC;
	/*Term Deposit*/
	INSERT INTO temp_documents_table(t_date,t_documenttype,t_documentdescription,t_clientname,t_filename,t_keyid,t_typeid,t_appid,t_bankname)
	SELECT c.LastUpdatetime,'Term Deposit','Term Deposit Application','N/A',CONCAT(c.UnsignedFileName,'!!~/App_Data/TD/barcodedocs/termdepositfiles/'),c.id,1003,a.id,d.BankName
	FROM application_history a LEFT JOIN Bank_Management b ON b.ApplicationID=a.id LEFT JOIN fund_mergeupdate c ON c.id=b.FundMergeUpdateID LEFT JOIN bankaccounttype d ON d.id=b.BankAccountTypeID
	WHERE a.Application_TypeID = 4 AND b.StatusID=2 AND d.BankName IS NOT NULL AND IFNULL(c.SignedFileName,0)=0 AND a.fundid=v_fundid ORDER BY c.LastUpdatetime DESC;
	
	/*ING*/
	INSERT INTO temp_documents_table(t_date,t_documenttype,t_documentdescription,t_clientname,t_filename,t_keyid,t_typeid,t_appid,t_bankname)
	SELECT c.LastUpdatetime,'ING Online Saver','ING Online Saver Application','N/A',CONCAT(c.UnsignedFileName,'!!~/App_Data/ING/'),c.id,1003,a.id,'ING Direct'
	FROM application_history a LEFT JOIN bank_management b ON b.applicationid=a.id LEFT JOIN fund_mergeupdate c ON b.FundMergeUpdateID=c.id
	WHERE a.Application_TypeID=6 AND b.statusid=2 AND IFNULL(a.isdeleted,0)=0 AND IFNULL(b.isdeleted,0)=0 AND a.fundid=v_fundid ORDER BY c.LastUpdatetime DESC;
	/*Change Broker*/
	INSERT INTO temp_documents_table(t_date,t_documenttype,t_documentdescription,t_clientname,t_filename,t_keyid,t_typeid,t_appid)
	SELECT  c.LastUpdatetime,'Change Broker','Change Broker Application','N/A',CONCAT(c.UnsignedFileName ,'!!~/App_Data/ChangeBrokerFile/'),c.id,1003,a.id
	FROM application_history a LEFT JOIN broker_management b ON b.applicationid=a.id LEFT JOIN fund_mergeupdate c ON c.id=b.FundMergeUpdateID
	WHERE b.applicationid=a.id AND c.id=b.FundMergeUpdateID AND a.Application_TypeID=2 AND b.statusid=2 AND IFNULL(a.isdeleted,0)=0 AND IFNULL(b.isdeleted,0)=0 AND a.fundid=v_fundid ORDER BY c.LastUpdatetime DESC;
	/*Death Benefit Agreements*/
	INSERT INTO temp_documents_table(t_date,t_documenttype,t_documentdescription,t_clientname,t_filename,t_keyid,t_typeid,t_appid,t_memberid)
	SELECT  a.ApplyDate,'Death Benefit Agreements','Death Benefit Agreements',CONCAT(b.FirstName,' ',b.LastName),CONCAT(c.UnsignedFileName ,'!!~/App_Data/BDADocs/'),c.id,1007,a.id,a.memberID
	FROM bda_application AS a,fundtrustee AS b,fund_mergeupdate AS c
	WHERE c.FundID=b.FundID AND c.FundID=a.fundID AND a.memberID=b.ID AND a.bda_mergeupdate_id=c.ID AND a.StatusID=3 AND a.fundID=v_fundid ORDER BY a.applydate DESC;
	/*COT*/
	INSERT INTO temp_documents_table(t_date,t_documenttype,t_documentdescription,t_clientname,t_filename,t_keyid,t_typeid,t_appid)
	SELECT c.lastupdatetime,'Applications', 'Trustee/Member Change Package','N/A', CONCAT(c.UnsignedFileName,'!!~/App_Data/ChangeTrusteeFile/SupportingDocuments/') AS filePath,c.id,1003,ah.id
	FROM application_history ah JOIN trustee_management trustee ON ah.id = trustee.applicationid JOIN fund_mergeupdate c ON trustee.`packageid`= c.id
	WHERE c.fundid=v_fundid AND trustee.StatusID=2 AND ah.application_typeid=3 AND ah.Isdeleted=0;
	/*Account Linkage Request*/
	INSERT INTO temp_documents_table(t_date,t_documenttype,t_documentdescription,t_clientname,t_filename,t_keyid,t_typeid, t_appid)	
	SELECT a.LastUpdateTime, 'Account Linkage Request', b.`Description`, 'N/A', a.`UnsignedFileName`, a.`ID`, c.ParentTypeID, a.id FROM fund_mergeupdate a 
	JOIN fund_merge_base_type b ON a.`TrustDeedTypeID` = b.`ID` 
	JOIN fund_document_base c ON b.DocumentBaseID = c.ID
	WHERE fundId = v_fundid AND (a.`IsSignedUploaded` = 0 OR a.`SignedFileName` IS NULL) AND c.`ParentTypeID` = 1016;
	/*SELECT a.LastUpdateTime, 'Account Linkage Request', b.`Description`, 'N/A', a.`UnsignedFileName`, a.`ID`, 1601, a.id FROM fund_mergeupdate a 
	JOIN fund_merge_base_type b
	ON a.`TrustDeedTypeID` = b.`ID` WHERE fundId = v_fundid AND (a.`IsSignedUploaded` = 0 OR a.`SignedFileName` IS NULL) AND b.`DocumentBaseID` = 1601;*/
	
	SELECT * FROM temp_documents_table;
	SELECT v_maxyear;
    END$$

DELIMITER ;