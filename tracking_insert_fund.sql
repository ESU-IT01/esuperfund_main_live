DELIMITER $$

DROP TRIGGER /*!50032 IF EXISTS */ `tracking_insert_fund`$$

CREATE TRIGGER `tracking_insert_fund` AFTER INSERT ON `fund` 
    FOR EACH ROW BEGIN
        INSERT INTO fund_change_tracking(table_name,action_flag,source_id,action_time)
        VALUES('fund','I',new.Id,NOW());
END;
$$

DELIMITER ;