DELIMITER $$

DROP TRIGGER /*!50032 IF EXISTS */ `tracking_update_bank_notmatched_accounts`$$

CREATE TRIGGER `tracking_update_bank_notmatched_accounts` AFTER UPDATE ON `bank_notmatched_accounts` 
    FOR EACH ROW BEGIN
     IF (new.Account <> old.Account)
     THEN
        INSERT INTO fund_change_tracking(table_name,action_flag,source_id,action_time)
        VALUES('bank_notmatched_accounts','U',new.Id,NOW());
     ELSEIF (new.MatchStatus = 2 AND old.MatchStatus in (1, 3, 4, 5))
     THEN
        INSERT INTO fund_change_tracking(table_name,action_flag,source_id,action_time)
        VALUES('bank_notmatched_accounts','I',new.Id,NOW());
     ELSEIF (new.MatchStatus = 1 AND old.MatchStatus = 2)
     THEN
        INSERT INTO fund_change_tracking(table_name,action_flag,source_id,action_time)
        VALUES('bank_notmatched_accounts','D',new.Id,NOW());
     END IF;
END;
$$

DELIMITER ;