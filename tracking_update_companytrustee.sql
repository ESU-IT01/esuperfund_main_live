DELIMITER $$

USE `esuperfund_main_live` $$

DROP TRIGGER /*!50032 IF EXISTS */ `tracking_update_companytrustee` $$

CREATE TRIGGER `tracking_update_companytrustee` AFTER UPDATE 
ON `fundcompanytrustee` FOR EACH ROW 
BEGIN
  IF (
    new.fundid <> old.fundid 
    OR IFNULL(new.CompanyName, '') <> IFNULL(old.CompanyName, '') 
    OR IFNULL(new.CompanyACN, '') <> IFNULL(old.CompanyACN, '') 
    OR IFNULL(new.TrusteeAppointed, '') <> IFNULL(old.TrusteeAppointed, '') 
    OR IFNULL(new.IsDeleted, '') <> IFNULL(old.IsDeleted, '') 
    OR IFNULL(new.IsResigned, '') <> IFNULL(old.IsResigned, '') 
    OR IFNULL(new.DateOfResigned, '') <> IFNULL(old.DateOfResigned, '') 
    OR IFNULL(new.CompanyEstDate, '') <> IFNULL(old.CompanyEstDate, '')
  ) 
  THEN 
  INSERT INTO fund_change_tracking (
    table_name,
    action_flag,
    source_id,
    action_time
  ) 
  VALUES
    (
      'fundcompanytrustee',
      'U',
      new.Id,
      NOW()
    ) ;
  END IF ;
END ;
$$

DELIMITER ;

