DELIMITER $$

USE `esuperfund_main_live` $$

DROP TRIGGER /*!50032 IF EXISTS */ `tracking_update_fundtrustee` $$

CREATE TRIGGER `tracking_update_fundtrustee` AFTER UPDATE 
ON `fundtrustee` FOR EACH ROW 
BEGIN
  IF (
    new.fundid <> old.fundid 
    OR IFNULL(new.FundTrusteeNo, '') <> IFNULL(old.FundTrusteeNo, '') 
    OR IFNULL(new.Title, '') <> IFNULL(old.Title, '') 
    OR IFNULL(new.FirstName, '') <> IFNULL(old.FirstName, '') 
    OR IFNULL(new.MiddleName, '') <> IFNULL(old.MiddleName, '') 
    OR IFNULL(new.LastName, '') <> IFNULL(old.LastName, '') 
    OR IFNULL(new.TrusteeTFN, '') <> IFNULL(old.TrusteeTFN, '') 
    OR IFNULL(new.DOB, '') <> IFNULL(old.DOB, '') 
    OR IFNULL(new.DateOfRetirement, '') <> IFNULL(old.DateOfRetirement, '') 
    OR IFNULL(new.IsMember, '') <> IFNULL(old.IsMember, '') 
    OR IFNULL(new.PlaceofBirth, '') <> IFNULL(old.PlaceofBirth, '') 
    OR IFNULL(new.TrusteeAppointed, '') <> IFNULL(old.TrusteeAppointed, '') 
    OR IFNULL(new.IsDeleted, '') <> IFNULL(old.IsDeleted, '') 
    OR IFNULL(new.RetirementStatus, '') <> IFNULL(old.RetirementStatus, '') 
    OR IFNULL(new.MemberDateofDeath, '') <> IFNULL(old.MemberDateofDeath, '') 
    OR IFNULL(new.MemberAppointed, '') <> IFNULL(old.MemberAppointed, '') 
    OR IFNULL(new.IsDeceased, '') <> IFNULL(old.IsDeceased, '') 
    OR IFNULL(new.IsResigned, '') <> IFNULL(old.IsResigned, '') 
    OR IFNULL(new.DateOfResigned, '') <> IFNULL(old.DateOfResigned, '') 
    OR IFNULL(new.TrustResidentialAddress, '') <> IFNULL(old.TrustResidentialAddress, '') 
    OR IFNULL(new.TrustResidentialSuburb, '') <> IFNULL(old.TrustResidentialSuburb, '') 
    OR IFNULL(new.TrustResidentialState, '') <> IFNULL(old.TrustResidentialState, '') 
    OR IFNULL(
      new.TrustResidentialPostcode,
      ''
    ) <> IFNULL(
      old.TrustResidentialPostcode,
      ''
    ) 
    OR IFNULL(new.TrustPostalAddress, '') <> IFNULL(old.TrustPostalAddress, '') 
    OR IFNULL(new.TrustPostalSuburb, '') <> IFNULL(old.TrustPostalSuburb, '') 
    OR IFNULL(new.TrustPostalState, '') <> IFNULL(old.TrustPostalState, '') 
    OR IFNULL(new.TrustPostalPostcode, '') <> IFNULL(old.TrustPostalPostcode, '') 
    OR IFNULL(new.Occupation, '') <> IFNULL(old.Occupation, '')
    OR IFNULL(new.TrustEmail, '') <> IFNULL(old.TrustEmail, '') 
    OR IFNULL(new.TrustLoginPassword, '') <> IFNULL(old.TrustLoginPassword, '') 
    OR IFNULL(new.TrustCountry, '') <> IFNULL(old.TrustCountry, '') 
    OR IFNULL(new.TrustJobType, '') <> IFNULL(old.TrustJobType, '') 
    OR IFNULL(new.IsCompanyShareHolder, '') <> IFNULL(old.IsCompanyShareHolder, '')
  ) 
  THEN 
  INSERT INTO fund_change_tracking (
    table_name,
    action_flag,
    source_id,
    action_time
  ) 
  VALUES
    ('fundtrustee', 'U', new.Id, NOW()) ;
  END IF ;
END ;
$$

DELIMITER ;

