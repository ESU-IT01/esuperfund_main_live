DELIMITER $$

DROP TRIGGER /*!50032 IF EXISTS */ `data_import_update_broker`$$

CREATE TRIGGER `data_import_update_broker` BEFORE UPDATE ON `fundbrokeraccount` 
    FOR EACH ROW 
BEGIN
    DECLARE fundBkId, bankType INT DEFAULT 0;	
	DECLARE oldOrderIndex, newOrderIndex INT DEFAULT 1;
	DECLARE curFundID INT DEFAULT 0;
		
	SET fundBkId = IFNULL(NEW.SettleBankId, 0);
	IF fundBkId > 0 THEN		
		IF NEW.BrokerName='CommSec' OR 
		NEW.BrokerName='CommSec Option' OR 
		NEW.BrokerName='CommSec CFDs' OR 
		NEW.BrokerName='CMC Markets' OR 
		NEW.BrokerName='Saxo Capital Markets' OR 
		NEW.BrokerName='EBROKING Options' OR 
		NEW.BrokerName='Bullion Capital' OR 
		NEW.BrokerName='FP Markets' OR 
		NEW.BrokerName='Interactive Brokers' OR
		NEW.BrokerName='EBROKING' THEN
			
			/* get fundid, orderindex */
			SELECT fundid, orderindex, BankAccountTypeID INTO curFundID, oldOrderIndex, bankType FROM fundbankaccount WHERE id = fundBkId;
			/* get orderindex based on expired status and update orderindex for all bank accounts of this fund */
			IF IFNULL(OLD.IsExpired, 0) = 0 AND NEW.IsExpired = 1 THEN -- broker moved to expired
				SET newOrderIndex = NULL; 
				UPDATE fundbankaccount SET 
				OrderIndex = OrderIndex - 1
				WHERE fundid = curFundID AND OrderIndex> oldOrderIndex;
			ELSEIF IFNULL(NEW.IsExpired, 0) = 0 AND OLD.IsExpired = 1 THEN -- broker moved to current
				SET newOrderIndex = 
				(
					SELECT IFNULL(COUNT(id),0) + 1 FROM fundbankaccount 
					WHERE fundid = curFundID AND IFNULL(IsDeleted, 0) = 0 
					AND IFNULL(IsExpired, 0) = 0 AND OrderIndex <> -1
				); 
			ELSE
				SET newOrderIndex = oldOrderIndex;
			END IF;
			
			/* update settle bank item */
			IF bankType = 6 OR bankType = 301 THEN
				UPDATE fundbankaccount SET 				
				IsDeleted = NEW.IsDeleted, 
				IsExpired = NEW.IsExpired, 
				ExpirationDate = NEW.ExpirationDate, 
				orderindex = newOrderIndex 
				WHERE id = fundBkId;
			ELSE
				UPDATE fundbankaccount SET 
				FundBankAccountNo = NEW.AccountNumber,
				IsDeleted = NEW.IsDeleted, 
				IsExpired = NEW.IsExpired, 
				ExpirationDate = NEW.ExpirationDate, 
				orderindex = newOrderIndex 
				WHERE id = fundBkId;
			END IF;          	
    	END IF;
	END IF;	
END;
$$

DELIMITER ;