DELIMITER $$

DROP TRIGGER /*!50032 IF EXISTS */ `tracking_insert_fundtrustee`$$

CREATE TRIGGER `tracking_insert_fundtrustee` AFTER INSERT ON `fundtrustee` 
    FOR EACH ROW BEGIN
       INSERT INTO fund_change_tracking(table_name,action_flag,source_id,action_time)
        VALUES('fundtrustee','I',new.Id,NOW());
END;
$$

DELIMITER ;