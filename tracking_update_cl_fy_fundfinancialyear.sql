DELIMITER $$

DROP TRIGGER /*!50032 IF EXISTS */ `tracking_update_cl_fy_fundfinancialyear`$$

CREATE TRIGGER `tracking_update_cl_fy_fundfinancialyear` AFTER UPDATE ON `cl_fy_fundfinancialyear` 
    FOR EACH ROW BEGIN
        /*tracking update cl_fy_fundfinancialyear*/
         IF (
           IFNULL(new.ID,'') <> IFNULL(old.ID,'')
           OR IFNULL(new.FundID,'') <> IFNULL(old.FundID,'')  
           OR IFNULL(new.Year,'') <> IFNULL(old.Year,'') 
           OR IFNULL(new.LodgementStatusID,'') <> IFNULL(old.LodgementStatusID,'')
		   OR IFNULL(new.LodgementDate, '') <> IFNULL(old.LodgementDate, '')
         )
         THEN
           INSERT INTO fund_change_tracking(table_name,action_flag,source_id,action_time)
           VALUES('cl_fy_fundfinancialyear','U',new.Id,NOW());
         END IF;
END;
$$

DELIMITER ;