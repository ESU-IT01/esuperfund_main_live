DELIMITER $$

DROP TRIGGER /*!50032 IF EXISTS */ `data_import_insert_broker`$$

CREATE TRIGGER `data_import_insert_broker` BEFORE INSERT ON `fundbrokeraccount` 
    FOR EACH ROW 
BEGIN
    DECLARE fundBkId INT DEFAULT 0;
	DECLARE fundOrderIndex INT DEFAULT 1;
	DECLARE fundBankType INT DEFAULT 0;    	
	
	IF NEW.AccountNumber IS NOT NULL AND NEW.AccountNumber <> '' THEN -- check the inserted broker account number
		/* assign value to variables */	
		SELECT IFNULL(COUNT(id),0)+1 INTO fundOrderIndex FROM fundbankaccount WHERE fundid=NEW.fundid AND IFNULL(IsDeleted,0)=0 AND IFNULL(IsExpired,0)=0;
		IF NEW.BrokerName='EBROKING'  AND IFNULL(NEW.SettleBankId,0)= 0  THEN
			SET fundBankType = 446;
			SELECT IFNULL(MIN(id),0) INTO fundBkId FROM fundbankaccount WHERE fundid=NEW.fundid AND IFNULL(IsDeleted,0)=0 AND IFNULL(IsExpired,0)=0 AND IFNULL(FundBankAccountNo,'0')=IFNULL(NEW.AccountNumber,'') AND BankAccountTypeID=fundBankType;
			IF fundBkId=0 THEN
				INSERT INTO fundbankaccount(FundID, BankAccountTypeID, FundBankAccountBSB, FundBankAccountNo,FundBankAccountName, CreateTime, OrderIndex, Onhold)
				VALUES(NEW.FundID, fundBankType, '', NEW.AccountNumber, 'EBROKING', NEW.CreationDate, fundOrderIndex, 0);
				
				SET fundBkId= LAST_INSERT_ID();
				SET NEW.SettleBankId = fundBkId;
			END IF;
		ELSEIF NEW.BrokerName='CommSec'  AND IFNULL(NEW.SettleBankId,0)= 0  THEN
			SET fundBankType = 226;
			SELECT IFNULL(MIN(id),0) INTO fundBkId FROM fundbankaccount WHERE fundid=NEW.fundid AND IFNULL(IsDeleted,0)=0 AND IFNULL(IsExpired,0)=0 AND IFNULL(FundBankAccountNo,'0')=IFNULL(NEW.AccountNumber,'') AND BankAccountTypeID=fundBankType;
			IF fundBkId=0 THEN
				INSERT INTO fundbankaccount(FundID, BankAccountTypeID, FundBankAccountBSB, FundBankAccountNo,FundBankAccountName, CreateTime, OrderIndex, Onhold)
				VALUES(NEW.FundID, fundBankType, '', NEW.AccountNumber, 'CommSec', NEW.CreationDate, fundOrderIndex, 0);
				
				SET fundBkId= LAST_INSERT_ID();
				SET NEW.SettleBankId = fundBkId;
			END IF;
		ELSEIF NEW.BrokerName='CommSec Option'  AND IFNULL(NEW.SettleBankId,0)= 0  THEN
			SET fundBankType = 260;
			SELECT IFNULL(MIN(id),0) INTO fundBkId FROM fundbankaccount WHERE fundid=NEW.fundid AND IFNULL(IsDeleted,0)=0 AND IFNULL(IsExpired,0)=0 AND IFNULL(FundBankAccountNo,'0')=IFNULL(NEW.AccountNumber,'') AND BankAccountTypeID=fundBankType;
			IF fundBkId=0 THEN
				INSERT INTO fundbankaccount(FundID, BankAccountTypeID, FundBankAccountBSB, FundBankAccountNo,FundBankAccountName, CreateTime, OrderIndex, Onhold)
				VALUES(NEW.FundID, fundBankType, '', NEW.AccountNumber, 'CommSec', NEW.CreationDate, fundOrderIndex, 0);
				
				SET fundBkId= LAST_INSERT_ID();
				SET NEW.SettleBankId = fundBkId;
			END IF;
		ELSEIF NEW.BrokerName='CommSec CFDs'  AND IFNULL(NEW.SettleBankId,0)= 0  THEN
			SET fundBankType = 261;
			SELECT IFNULL(MIN(id),0) INTO fundBkId FROM fundbankaccount WHERE fundid=NEW.fundid AND IFNULL(IsDeleted,0)=0 AND IFNULL(IsExpired,0)=0 AND IFNULL(FundBankAccountNo,'0')=IFNULL(NEW.AccountNumber,'') AND BankAccountTypeID=fundBankType;
			IF fundBkId=0 THEN
				INSERT INTO fundbankaccount(FundID, BankAccountTypeID, FundBankAccountBSB, FundBankAccountNo,FundBankAccountName, CreateTime, OrderIndex, Onhold)
				VALUES(NEW.FundID, fundBankType, '', NEW.AccountNumber, 'CommSec', NEW.CreationDate, fundOrderIndex, 0);
				
				SET fundBkId= LAST_INSERT_ID();
				SET NEW.SettleBankId = fundBkId;
			END IF;
		ELSEIF NEW.BrokerName='CMC Markets'  AND IFNULL(NEW.SettleBankId,0)= 0  THEN
			SET fundBankType = 262;
			SELECT IFNULL(MIN(id),0) INTO fundBkId FROM fundbankaccount WHERE fundid=NEW.fundid AND IFNULL(IsDeleted,0)=0 AND IFNULL(IsExpired,0)=0 AND IFNULL(FundBankAccountNo,'0')=IFNULL(NEW.AccountNumber,'') AND BankAccountTypeID=fundBankType;
			IF fundBkId=0 THEN
				INSERT INTO fundbankaccount(FundID, BankAccountTypeID, FundBankAccountBSB, FundBankAccountNo,FundBankAccountName, CreateTime, OrderIndex, Onhold)
				VALUES(NEW.FundID, fundBankType, '', NEW.AccountNumber, 'CMC', NEW.CreationDate, fundOrderIndex, 0);
				
				SET fundBkId= LAST_INSERT_ID();
				SET NEW.SettleBankId = fundBkId;
			END IF;
		ELSEIF NEW.BrokerName='Saxo Capital Markets'  AND IFNULL(NEW.SettleBankId,0)= 0  THEN
			SET fundBankType = 263;
			SELECT IFNULL(MIN(id),0) INTO fundBkId FROM fundbankaccount WHERE fundid=NEW.fundid AND IFNULL(IsDeleted,0)=0 AND IFNULL(IsExpired,0)=0 AND IFNULL(FundBankAccountNo,'0')=IFNULL(NEW.AccountNumber,'') AND BankAccountTypeID=fundBankType;
			IF fundBkId=0 THEN
				INSERT INTO fundbankaccount(FundID, BankAccountTypeID, FundBankAccountBSB, FundBankAccountNo,FundBankAccountName, CreateTime, OrderIndex, Onhold)
				VALUES(NEW.FundID, fundBankType, '', NEW.AccountNumber, 'Saxo', NEW.CreationDate, fundOrderIndex, 0);
				
				SET fundBkId= LAST_INSERT_ID();
				SET NEW.SettleBankId = fundBkId;
			END IF;
		ELSEIF NEW.BrokerName='EBROKING Options'  AND IFNULL(NEW.SettleBankId,0)=0  THEN
			SET fundBankType = 298;
			SELECT IFNULL(MIN(id),0) INTO fundBkId FROM fundbankaccount WHERE fundid=NEW.fundid AND IFNULL(IsDeleted,0)=0 AND IFNULL(IsExpired,0)=0 AND IFNULL(FundBankAccountNo,'0')=IFNULL(NEW.AccountNumber,'') AND BankAccountTypeID=fundBankType;
			IF fundBkId=0 THEN
				INSERT INTO fundbankaccount(FundID, BankAccountTypeID, FundBankAccountBSB, FundBankAccountNo,FundBankAccountName, CreateTime, OrderIndex, Onhold)
				VALUES(NEW.FundID, fundBankType, '', NEW.AccountNumber, 'EBROKING', NEW.CreationDate, fundOrderIndex, 0);
				
				SET fundBkId= LAST_INSERT_ID();
				SET NEW.SettleBankId = fundBkId;
			END IF;
		ELSEIF NEW.BrokerName='Bullion Capital'  AND IFNULL(NEW.SettleBankId,0)= 0  THEN
			SET fundBankType = 305;
			SELECT IFNULL(MIN(id),0) INTO fundBkId FROM fundbankaccount WHERE fundid=NEW.fundid AND IFNULL(IsDeleted,0)=0 AND IFNULL(IsExpired,0)=0 AND IFNULL(FundBankAccountNo,'0')=IFNULL(NEW.AccountNumber,'') AND BankAccountTypeID=fundBankType;
			IF fundBkId=0 THEN
				INSERT INTO fundbankaccount(FundID, BankAccountTypeID, FundBankAccountBSB, FundBankAccountNo,FundBankAccountName, CreateTime, OrderIndex, Onhold)
				VALUES(NEW.FundID, fundBankType, '', NEW.AccountNumber, '', NEW.CreationDate, fundOrderIndex, 0);
				
				SET fundBkId= LAST_INSERT_ID();
				SET NEW.SettleBankId = fundBkId;
			END IF;
		ELSEIF NEW.BrokerName='FP Markets'  AND IFNULL(NEW.SettleBankId,0)= 0  THEN
			SET fundBankType = 309;
			SELECT IFNULL(MIN(id),0) INTO fundBkId FROM fundbankaccount WHERE fundid=NEW.fundid AND IFNULL(IsDeleted,0)=0 AND IFNULL(IsExpired,0)=0 AND IFNULL(FundBankAccountNo,'0')=IFNULL(NEW.AccountNumber,'') AND BankAccountTypeID=fundBankType;
			IF fundBkId=0 THEN
				INSERT INTO fundbankaccount(FundID, BankAccountTypeID, FundBankAccountBSB, FundBankAccountNo,FundBankAccountName, CreateTime, OrderIndex, Onhold)
				VALUES(NEW.FundID, fundBankType, '', NEW.AccountNumber, '', NEW.CreationDate, fundOrderIndex, 0);
				
				SET fundBkId= LAST_INSERT_ID();
				SET NEW.SettleBankId = fundBkId;
			END IF;
		ELSEIF NEW.BrokerName='Interactive Brokers'  AND IFNULL(NEW.SettleBankId,0)= 0  THEN
			SET fundBankType = 327;
			SELECT IFNULL(MIN(id),0) INTO fundBkId FROM fundbankaccount WHERE fundid=NEW.fundid AND IFNULL(IsDeleted,0)=0 AND IFNULL(IsExpired,0)=0 AND IFNULL(FundBankAccountNo,'0')=IFNULL(NEW.AccountNumber,'') AND BankAccountTypeID=fundBankType;
			IF fundBkId=0 THEN
				INSERT INTO fundbankaccount(FundID, BankAccountTypeID, FundBankAccountBSB, FundBankAccountNo,FundBankAccountName, CreateTime, OrderIndex, Onhold)
				VALUES(NEW.FundID, fundBankType, '', NEW.AccountNumber, '', NEW.CreationDate, fundOrderIndex, 0);
				
				SET fundBkId= LAST_INSERT_ID();
				SET NEW.SettleBankId = fundBkId;
			END IF;
		END IF;
	END IF;
END;
$$

DELIMITER ;