DELIMITER $$

DROP TRIGGER /*!50032 IF EXISTS */ `tracking_insert_companytrustee`$$

CREATE TRIGGER `tracking_insert_companytrustee` AFTER INSERT ON `fundcompanytrustee` 
    FOR EACH ROW BEGIN
        INSERT INTO fund_change_tracking(table_name,action_flag,source_id,action_time)
        VALUES('fundcompanytrustee','I',new.Id,NOW());
END;
$$

DELIMITER ;