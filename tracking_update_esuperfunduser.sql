DELIMITER $$

DROP TRIGGER /*!50032 IF EXISTS */ `tracking_update_esuperfunduser`$$

CREATE TRIGGER `tracking_update_esuperfunduser` AFTER UPDATE ON `esuperfunduser` 
    FOR EACH ROW BEGIN
       /*tracking update esuperfunduser` */
	   IF (
           IFNULL(new.UserName,'')<> IFNULL(old.UserName,'')
           OR IFNULL(new.NewPassword,'') <> IFNULL(old.NewPassword,'')  
           OR IFNULL(new.EmailAddress,'') <> IFNULL(old.EmailAddress,'')		   	   
		   OR IFNULL(new.IsLogin,'') <> IFNULL(old.IsLogin,'')
		   OR IFNULL(new.LoginIP,'') <> IFNULL(old.LoginIP,'')
		   OR IFNULL(new.LastUpdateTime,'') <> IFNULL(old.LastUpdateTime,'')
		   OR IFNULL(new.IsNeedToVerifyIP,'') <> IFNULL(old.IsNeedToVerifyIP,'')
		   OR IFNULL(new.GUID,'') <> IFNULL(old.GUID,'')
		   OR IFNULL(new.IsVerified,'') <> IFNULL(old.IsVerified,'')
		   OR IFNULL(new.DepartmentID,'') <> IFNULL(old.DepartmentID,'')
		   OR IFNULL(new.InboxRoleID,'') <> IFNULL(old.InboxRoleID,'')
		   OR IFNULL(new.JobTitle,'') <> IFNULL(old.JobTitle,'')
		   OR IFNULL(new.IsArchived,'') <> IFNULL(old.IsArchived,'')
		   OR IFNULL(new.MobileNumber,'') <> IFNULL(old.MobileNumber,'')
		   )
		THEN
			INSERT INTO fund_change_tracking(table_name, action_flag, source_id, action_time)
			VALUES ('esuperfunduser','U',new.Id, NOW());
		END IF;
END;
$$

DELIMITER ;