DELIMITER $$

DROP TRIGGER /*!50032 IF EXISTS */ `tracking_insert_cl_fy_fundfinancialyear`$$

CREATE TRIGGER `tracking_insert_cl_fy_fundfinancialyear` AFTER INSERT ON `cl_fy_fundfinancialyear` 
    FOR EACH ROW BEGIN
        INSERT INTO fund_change_tracking(table_name,action_flag,source_id,action_time)
        VALUES('cl_fy_fundfinancialyear','I',new.Id,NOW());
END;
$$

DELIMITER ;