DELIMITER $$

DROP TRIGGER /*!50032 IF EXISTS */ `tracking_insert_esuperfunduser`$$

CREATE TRIGGER `tracking_insert_esuperfunduser` AFTER INSERT ON `esuperfunduser` 
    FOR EACH ROW BEGIN
        INSERT INTO fund_change_tracking(table_name,action_flag,source_id,action_time)
        VALUES('esuperfunduser','I',new.Id,NOW());
END;
$$

DELIMITER ;