DELIMITER $$

DROP TRIGGER /*!50032 IF EXISTS */ `tracking_insert_brokeraccount`$$

CREATE TRIGGER `tracking_insert_brokeraccount` AFTER INSERT ON `fundbrokeraccount` 
    FOR EACH ROW BEGIN
           INSERT INTO fund_change_tracking(table_name,action_flag,source_id,action_time)
        VALUES('fundbrokeraccount','I',new.Id,NOW());
END;
$$

DELIMITER ;